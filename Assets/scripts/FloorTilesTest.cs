﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorTilesTest : MonoBehaviour {

	// Use this for initialization
	public Transform floortile;

	private int scale = 5;

	void Start() {

		//scale = DataManager.instance.GetPlayerCount ();

      scale = DataManager.instance.GetComplexNumber();
      
		for(int z = 0; z < scale; z++) {
			for(int x = 0; x < scale; x++) {
				Instantiate(floortile, new Vector3(x, 0, z), Quaternion.Euler(90, 0, 0));
			}
		}
	}

	// Update is called once per frame
	void Update () {

	}
		
}