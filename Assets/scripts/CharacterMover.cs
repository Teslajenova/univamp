﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMover : MonoBehaviour {

	public Transform character;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void FixedUpdate(){
		character.Translate (
			Time.deltaTime * 1f * Input.GetAxis("Horizontal"),
			0,
			Time.deltaTime * -1f * Input.GetAxis("Horizontal"),
			Space.World
		);

		character.Translate (
			Time.deltaTime * 1f * Input.GetAxis("Vertical"),
			0,
			Time.deltaTime * 1f * Input.GetAxis("Vertical"),
			Space.World
		);
	}
}
