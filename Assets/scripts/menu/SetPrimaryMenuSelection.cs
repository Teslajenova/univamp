﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SetPrimaryMenuSelection : MonoBehaviour {

  public EventSystem eventSystem;

  public VerticalLayoutGroup menu;

  public void SetSelection() {
    eventSystem.SetSelectedGameObject(menu.GetComponentsInChildren<Selectable>()[0].gameObject);
  }
}
