using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Text;
using Vanitas.Components.Attributes;
using Vanitas.Components;
using System;

public class CharacterSelectInit : MonoBehaviour {

  public EventSystem eventSystem;

  public CanvasRenderer playerPanel;
  public HorizontalLayoutGroup parentGroup;

  private int playerCount = 4; // default

  
  // Use this for initialization
  void Start () {
    SetPlayerCount();

    InitPlayerPanels();
    InitCharacterPickers();
    InitClanPickers();

    SetInitialSelection();
  }

  
  private int SetPlayerCount() {
    
    if(Data.GetCity() != null){
      playerCount = Data.GetCity().NumberOfPlayers;
    }

    return playerCount;
  }
  
  
  private void InitPlayerPanels() {
    for (int i = 0; i < playerCount; i++) {
      CanvasRenderer panel = Instantiate(playerPanel);
      panel.transform.SetParent(parentGroup.transform, false);
    }
  }

  
  private void InitCharacterPickers() {
    HashSet<Character> mortals = Data.GetCity().GetMortals();
    
    for(int j = 0; j < playerCount; j++) {
      Dropdown characterPicker = parentGroup.GetComponentsInChildren<VerticalLayoutGroup>()[j].transform.Find("CharacterPicker").GetComponent<Dropdown>();

      foreach(Character character in mortals){
        characterPicker.options.Add(new Dropdown.OptionData(character.Name));
      }
      
      /*
      for(int i = 0; i < 20; i++) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.Append("Dummy Option ").Append(i);

        //characterPicker.options.Add(new Dropdown.OptionData(stringBuilder.ToString(), Resources.Load("painter", typeof(Sprite)) as Sprite));
        characterPicker.options.Add(new Dropdown.OptionData(stringBuilder.ToString()));
      }
      */ 
      characterPicker.RefreshShownValue();
    }
  }

  
  private void InitClanPickers() {
    for(int j = 0; j < playerCount; j++) {
      Dropdown clanPicker = parentGroup.GetComponentsInChildren<VerticalLayoutGroup>()[j].transform.Find("ClanPicker").GetComponent<Dropdown>();

      foreach(Clan clan in Enum.GetValues(typeof(Clan))){
      	clanPicker.options.Add(new Dropdown.OptionData(clan.ToString()));
      }
      
      /*
      for(int i = 0; i < 7; i++) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.Append("Clan ").Append(i);

        clanPicker.options.Add(new Dropdown.OptionData(stringBuilder.ToString()));
      }
      */ 
      
      clanPicker.RefreshShownValue();
    }
  }

  
  private void SetInitialSelection() {
    eventSystem.SetSelectedGameObject(parentGroup.GetComponentsInChildren<Dropdown>()[0].gameObject);
  }

}