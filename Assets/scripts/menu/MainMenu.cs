﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;
using UnityEngine.SceneManagement;
using Vanitas.Components;

public class MainMenu : MonoBehaviour {

  
  private static readonly int CHARACTER_SELECT_SCENE_INDEX = 1;

  public Button menuButton;
  public VerticalLayoutGroup menu;

  
  // Use this for initialization
  void Start () {
    PlayerSelectMenuSetup();
  }
  
  
  // Update is called once per frame
  void Update () {
    
  }

  
  private void PlayerSelectMenuSetup() {
    int options = Config.MAX_NUMBER_OF_PLAYERS;

    for (int i = 0; i < options; i++) {
      AddPLayerSelectButton(i +1);
    }
  }

  
  private void AddPLayerSelectButton(int playerCount) {
    // create text
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.Append(playerCount);
    stringBuilder.Append(" Player");
    if (playerCount > 1) {
      stringBuilder.Append("s");
    }

    // set button
    Button button = Instantiate(menuButton);
    button.transform.SetParent(menu.transform, false);
    button.GetComponentInChildren<Text>().text = stringBuilder.ToString();
    button.transform.SetSiblingIndex(playerCount -1);
    button.onClick.AddListener(delegate { MoveToNextScene(playerCount); });
  }

  
  void MoveToNextScene(int playerCount) {

    Data data = Data.INSTANCE;

    Data.INSTANCE.City = new City(playerCount);
    Data.GetCity().InitCharacters();
    
    //DataManager.instance.selectedPlayerCount = playerCount;
    SceneManager.LoadScene(CHARACTER_SELECT_SCENE_INDEX);
  }
  
  
}