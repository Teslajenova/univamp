﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowingCamera : MonoBehaviour {

	public Transform target;
	public Transform camera;


	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		camera.position = new Vector3 (
			target.position.x -3,
			target.position.y +3,
			target.position.z -3
		);

	}
}
