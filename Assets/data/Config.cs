using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Config : MonoBehaviour {

  //public static Config INSTANCE;

  public static readonly int MAX_NUMBER_OF_PLAYERS = 4;

  public static readonly int INITIAL_CITIZENS = 200;

  public static readonly int INITIAL_NPC_VAMPIRES = 9;

  public static readonly int DEFAULT_GENERATION = 13;

  public static readonly int TERMINAL_GENERATION = 4;

  public static readonly int MAX_INITIAL_TRAITS = 3;

  public static readonly float UPPER_CLASS_RATIO = 0.15f;

  public static readonly float LOWER_CLASS_RATIO = 0.25f;

  public static readonly int NEW_VS_OLD_CHARACTER_RANDOMIZER = 10;

  public static readonly int ALLY_STAND_RANDOMIZER = 10;

  public static readonly int ACQUAINTANCE_STAND_RANDOMIZER = 6;

  public static readonly int NEXT_NEED_RANDOMIZER = 5;

  public static readonly int RANDOM_EXTRA_ACQUAINTANCES = 5;

  public static readonly int COMMON_ERG_SPECTRUM_MIN = 1;

  public static readonly int COMMON_ERG_SPECTRUM_MAX = 3;

  /*
  void Awake(){
    if (INSTANCE == null) {
      INSTANCE = this;
    } else if (INSTANCE != this) {
      Destroy(gameObject);
    }

    DontDestroyOnLoad(gameObject);
  }
  
  public static Config Instance{
    get{
      return INSTANCE;
    }
  }
  */

}