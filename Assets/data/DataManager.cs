﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vanitas.Components;
using Vanitas.Test;

public class DataManager : MonoBehaviour {

  private City city;
  
  public City City{
    get{
      return city;
    }
    set{
      city = value;
    }
  }
  

  //TODO: move these to another class or figure out another approach
  public static int RND(int max) {
    return Random.Range(0, max);
  }

  public static int RND(int min, int max) {
    return Random.Range(min, max);
  }


  ////

  public int selectedPlayerCount = 4;

  public static DataManager instance;

  private ComplexOne complexOne = new ComplexOne(10);
  
	private int playerCount = 0;

	public void SetPlayerCount(int playerCount){
		this.playerCount = playerCount;
	}

	public int GetPlayerCount(){
		return this.playerCount;
	}

  public int GetComplexNumber(){
		return complexOne.Number;
	}
  
  ////

	void Awake(){
		if (instance == null) {
			instance = this;
		} else if (instance != this) {
			Destroy (gameObject);
		}

		DontDestroyOnLoad (gameObject);
	}
		
}