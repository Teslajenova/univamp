using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vanitas.Components;

public class Data : MonoBehaviour {
  
  private City city;
  
  
  public City City{
    get{
      return city;
    }
    set{
      city = value;
    }
  }
  
  
  public static Data INSTANCE;

  
  public static City GetCity(){
    if(INSTANCE == null){
      return null;
    }
    
    return INSTANCE.City;
  }
  
  
  void Awake(){
    if (INSTANCE == null) {
      INSTANCE = this;
    } else if (INSTANCE != this) {
      Destroy(gameObject);
    }

    DontDestroyOnLoad(gameObject);
  }

  
}