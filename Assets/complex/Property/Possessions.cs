using System.Collections.Generic;
using Vanitas.Components.Attributes;

namespace Vanitas.Components.Property{
  public class Possessions{
    
    private int capacity = 0;
    private HashSet<Item> items = new HashSet<Item>();
    
    
    public Possessions(Character character){
      InitCapacity(character);
    }
    
    public int ClosedCapacity(){
      int closed = 0;
      
      foreach(Item item in items){
        closed += item.Worth;
      }
      
      return closed;
    }
    
    public int OpenCapacity(){
      return capacity - ClosedCapacity();
    }
    
    private void InitCapacity(Character character){
      this.capacity = (int)Stat.Resources.Data().RandomValidCountForValue(character.GetStat(Stat.Resources));
    }
    
    public int Capacity{
      get{
        return this.capacity;
      }
    }
    
  }
}