namespace Vanitas.Components.Property{
  public abstract class Item{
    
    protected int baseWorth;
    
    public Item(int baseWorth){
      this.baseWorth = baseWorth;
    }
    
    public int Worth{
      get{
        return this.baseWorth;
      }
    }
    
  }
}