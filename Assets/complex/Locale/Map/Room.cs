
using Assets.complex.Base;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Vanitas.Components.Locale.Map{
  public class Room{
    
    private RoomType roomType;
    private int width;
    private int length;
    
    private HashSet<Node> nodes = new HashSet<Node>();
    
    private HashSet<Room> adjoined = new HashSet<Room>();
    private HashSet<Room> adjacent = new HashSet<Room>();
    
    //todo: add orientation? bool rotated?
    
    public Room(RoomType roomType){
      this.roomType = roomType;
      Point dimensions = roomType.Data().GetRandomDimensions();
      
      int rotate = DataManager.RND(1);
      
      if (rotate == 0){
        width = dimensions.X;
        length = dimensions.Y;
      }else{
        width = dimensions.Y;
        length = dimensions.X;
      }
      
      MakeNodeRectangle(width, length);
      
      RandomizeOrigin();
    }
    
    
    private void MakeNodeRectangle(Point point){
      MakeNodeRectangle(point.X, point.Y);
    }
    
    
    private void MakeNodeRectangle(int width, int length){
      
      for(int x = 0; x < width; x++){
        for(int y = 0; y < length; y++){
          Point coordinates = new Point(x,y);
          
          NodeType type = NodeType.Common;
          
          if((x == 0 || x == width-1) && (y == 0 || y == length-1)){
            type = NodeType.Corner;
          }else if(x == 0 || x == width-1 || y == 0 || y == length-1){
            type = NodeType.Edge;
          }
          
          Node node = new Node(coordinates, type);                 
                               
          nodes.Add(node);
        }
      }
      
    }
    
    
    private void RandomizeOrigin(){
      Move(0 - DataManager.RND(width), 0 - DataManager.RND(length));
    }
    
    
    public void Move(Point point){
      Move(point.X, point.Y);
    }
    
    
    public void Move(int x, int y){
      foreach(Node node in nodes){
        node.Coordinates = new Point(node.Coordinates.X + x, node.Coordinates.Y + y);
      }
    }
    
    
    // todo: add rotate method?
    
    public HashSet<Node> GetNodes(){
      return new HashSet<Node>(nodes);
    }
    
    
    public Node GetRandomNode(){
      return nodes.ToList()[DataManager.RND(nodes.Count)];
    }
    
    
    public HashSet<Node> GetNodes(NodeType nodeType){
      HashSet<Node> iNodes = new HashSet<Node>();
      
      foreach(Node node in nodes){
        if(node.Type == nodeType){
          iNodes.Add(node);
        }
      }
      
      return iNodes;
    }
    
    
    public HashSet<Node> GetBoundaryNodes(){
      HashSet<Node> bNodes = new HashSet<Node>();
      
      foreach(Node node in nodes){
        if(node.Type == NodeType.Edge || node.Type == NodeType.Corner){
          bNodes.Add(node);
        }
      }
      
      return bNodes;
    }
    
    
    public HashSet<Node> GetBoundaryNodes(Direction direction){
      List<Node> bNodes = new List<Node>();
      
      foreach(Node node in nodes){
        if(bNodes.Count == 0){
          bNodes.Add(node);
          continue;
        }
        
        if(((direction == Direction.West || direction == Direction.East) && node.Coordinates.X == bNodes[0].Coordinates.X) ||
          ((direction == Direction.North || direction == Direction.South) && node.Coordinates.Y == bNodes[0].Coordinates.Y)){
          bNodes.Add(node);
          continue;
        }
        
        if((direction == Direction.West && node.Coordinates.X < bNodes[0].Coordinates.X) ||
          (direction == Direction.East && node.Coordinates.X > bNodes[0].Coordinates.X) ||
          (direction == Direction.North && node.Coordinates.Y < bNodes[0].Coordinates.Y) ||
          (direction == Direction.South && node.Coordinates.Y > bNodes[0].Coordinates.Y)){
          bNodes.Clear();
          bNodes.Add(node);
        }
        
      }

      return new HashSet<Node>(bNodes);
    }
    
    
    public void AddAdjoined(Room room){
      adjoined.Add(room);
    }
    
    
    public void AddAdjacent(Room room){
      adjacent.Add(room);
    }
    
    
    public HashSet<Node> Nodes{
      get{
        return new HashSet<Node>(this.nodes);
      }
    }
    
    
    public HashSet<Room> Adjoined{
      get{
        return new HashSet<Room>(this.adjoined);
      }
    }
    
    
    public HashSet<Room> Adjacent{
      get{
        return new HashSet<Room>(this.adjacent);
      }
    }
                   
    
    public RoomType Type{
      get{
        return roomType;
      }
    }
    
  }
}