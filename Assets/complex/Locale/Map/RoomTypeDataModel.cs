using Assets.complex.Base;

namespace Vanitas.Components.Locale.Map{
  public class RoomTypeDataModel{
    
    private string name; // room type identifier
    
    private int minWidth;
    private int maxWidth;
    private int minLength;
    private int maxLength;
    
    private PortalType defaultPortalType;
    
    // add floor tile texture
    
    
    public RoomTypeDataModel(string name, int minWidth, int maxWidth, int minLength, int maxLength, PortalType defaultPortalType){
      this.name = name;
      this.minWidth = minWidth;
      this.maxWidth = maxWidth;
      this.minLength = minLength;
      this.maxLength = maxLength;
      this.defaultPortalType = defaultPortalType;
    }
    
    
    public Point GetRandomDimensions(){
      int width = DataManager.RND(minWidth, maxWidth);
      int length = DataManager.RND(minLength, maxLength);
      return new Point(width, length);
    }
    
    
    public PortalType PortalType{
      get{
        return defaultPortalType;
      }
    }
    
  }
}