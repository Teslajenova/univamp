

using Assets.complex.Base;

namespace Vanitas.Components.Locale.Map{
  public class DirectionDataModel{
    
    private bool polar;
    private bool positive;
    private Direction opposite;
    
    public DirectionDataModel(bool polar, bool positive, Direction opposite){
      this.polar = polar;
      this.positive = positive;
      this.opposite = opposite;
    }
    
    
    public Point Offset(Point originalPoint, int amount){
      int modifier = 1;
      if(!positive){
        modifier = -1;
      }
      
      if(polar){
        return new Point(originalPoint.X, originalPoint.Y + (amount * modifier));
      }else{
        return new Point(originalPoint.X + (amount * modifier), originalPoint.Y);
      }
    }
    
    
    public bool IsPolar(){
      return polar;
    }
    
    
    public bool IsPositive(){
      return positive;
    }
    
    
    public Direction Opposite(){
      return opposite;
    }
    
  }
}