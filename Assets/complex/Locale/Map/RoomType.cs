using System.Collections.Generic;

namespace Vanitas.Components.Locale.Map{
  public enum RoomType{
    Shop,
    DiningArea,
    Kitchen,
    Restroom,
    //Bathroom,
    //Toilet,
    Storeroom,
    StaffRoom
  }
  
  public static class RoomTypeExtension{
    
    public static RoomTypeDataModel Data(this RoomType roomType){
      return DATA_MODELS[roomType];
    }

    
    public static Dictionary<RoomType, RoomTypeDataModel> DATA_MODELS = new Dictionary<RoomType, RoomTypeDataModel>(){
      {
        RoomType.Shop,
        new RoomTypeDataModel(
          "Shop",
          5,10, //width
          8,15,  //length
          PortalType.OneWayDoor
        )
      },
      {
        RoomType.DiningArea,
        new RoomTypeDataModel(
          "DiningArea",
          6,14, //width
          8,16,  //length
          PortalType.Passage
        )
      },
      {
        RoomType.Kitchen,
        new RoomTypeDataModel(
          "Kitchen",
          5,10, //width
          6,17,  //length
          PortalType.TwoWayDoor
        )
      },
      {
        RoomType.Restroom,
        new RoomTypeDataModel(
          "Restroom",
          4,7, //width
          5,10,  //length
          PortalType.TwoWayDoor
        )
      },
      {
        RoomType.Storeroom,
        new RoomTypeDataModel(
          "Storeroom",
          4,10, //width
          8,20,  //length
          PortalType.OneWayDoor
        )
      },
      {
        RoomType.StaffRoom,
        new RoomTypeDataModel(
          "Staff Room",
          3,7, //width
          4,10,  //length
          PortalType.TwoWayDoor
        )
      }
    };
    
  }
  
}