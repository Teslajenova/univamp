
using Assets.complex.Base;
using System.Collections.Generic;
using System.Linq;

namespace Vanitas.Components.Locale.Map{
  public class Portal{
    
    
    private PortalType type;
    private HashSet<Node> nodes = new HashSet<Node>();
    
    // probably should have reference to connected rooms
    
    
    public Portal(PortalType type): this(type, new Point(0,0)){
    
    }
    
    
    public Portal(PortalType type, Point point){
      this.type = type;
      nodes.Add(new Node(point, type.Data().NodeType));
    }
    
    
    /*
    public Portal(NodeType nodeType){
      Point point = new Point(0,0);
      Node node = new Node(point, nodeType);
      nodes.Add(node);
    }
    */
    
    
    public HashSet<Node> Nodes{
      get{
        return new HashSet<Node>(this.nodes);
      }
    }
    
    
    public void PlaceAt(Point point){
      Node rootNode = nodes.ToList()[0];
      //note: all nodes need to be identical or we need to use a list
      rootNode.Coordinates = point;
      
      nodes.Clear();
      nodes.Add(rootNode);
    }
    
  }
}