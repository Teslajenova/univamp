namespace Vanitas.Components.Locale.Map{
  public enum NodeType{
    Common,
    Edge,
    Corner,
    
    Exit,
    Portal
  }
}