
using Assets.complex.Base;
using System.Collections.Generic;

namespace Vanitas.Components.Locale.Map{
  public class Grid{
    
    private HashSet<Node> nodes = new HashSet<Node>();
    
    
    public Grid(LocationMap map): this(map.Rooms, map.Portals) {

    }

    public Grid(HashSet<Room> rooms, HashSet<Portal> portals){
      
      foreach(Room room in rooms){
        this.nodes.UnionWith(room.Nodes);
      }
      
      foreach(Portal portal in portals){
        this.nodes.UnionWith(portal.Nodes);
      }
      
    }
    
    
    public HashSet<Node> Nodes{
      get{
        return new HashSet<Node>(nodes);
      }
    }
    
    
    public Node GetNodeAt(Point point){
      return GetNodeAt(point.X, point.Y);
    }
    
    
    public Node GetNodeAt(int cx, int cy){
      foreach(Node node in nodes){
        if(node.Coordinates.X == cx && node.Coordinates.Y == cy){
          return node;
        }
      }
      
      return null;
    }
    
    
    public Node GetNodeFrom(Node node, Direction direction, int amount){
      if(node == null || direction == null){
        return null;
      }
      
      Point derivedPoint = direction.Data().Offset(node.Coordinates, amount);
      
      return GetNodeAt(derivedPoint);
      
    }
    
    
    public HashSet<Node> GetSurroundingNodes(Point point, Direction direction, bool inclusive){
      HashSet<Node> all = GetSurroundingNodes(point);
      
      HashSet<Node> filtered = new HashSet<Node>();
      
      int refVal = point.X;
      if(direction.Data().IsPolar()){
        refVal = point.Y;
      }
      
      int modifier = -1;
      if(direction.Data().IsPositive()){
        modifier = 1;
      }
      
      refVal *= modifier;
      
      foreach(Node node in all){
        int val = node.Coordinates.X;
        if(direction.Data().IsPolar()){
          val = node.Coordinates.Y;
        }
        
        val *= modifier;
        
        bool inside = (val > refVal);
        
        if(inside == inclusive){
          filtered.Add(node);
        }
        
      }
      
      return filtered;
    }
    
    
    public HashSet<Node> GetSurroundingNodes(Point point){
      HashSet<Node> sNodes = new HashSet<Node>();
      
      foreach(Node node in nodes){
        if((node.Coordinates.X >= point.X-1 && node.Coordinates.X <= point.X+1) 
           && (node.Coordinates.Y >= point.Y-1 && node.Coordinates.Y <= point.Y+1) 
           && !node.Coordinates.Equals(point)){
          sNodes.Add(node);
        }
      }
      
      return sNodes;
    }
    
  }
}