

using Assets.complex.Base;

namespace Vanitas.Components.Locale.Map{
  public class Node{
    
    private Point coordinates;
    private NodeType type;
    
    //private bool isEdgePiece = false;
    //private bool isCornerPiece = false;
    
    public Node(Point coordinates, NodeType type){
      this.coordinates = coordinates;
      this.type = type;
    }
    
    public Point Coordinates{
      get{
        return new Point(coordinates.X, coordinates.Y);
      }
      set{
        this.coordinates = value;
      }
    }
    
    public NodeType Type{
      get{
        return this.type;
      }
    }
    
    /*
    public bool IsEdgePiece{
      get;
      set;
    }
    
    public bool IsCornerPiece{
      get;
      set;
    }
    */
    
  }
}