using System.Collections.Generic;

namespace Vanitas.Components.Locale.Map{
  public enum PortalType{
    Passage,
    OneWayDoor,
    TwoWayDoor,
    Exit
  }
  
  public static class PortalTypeExtension{
    
    public static PortalTypeDataModel Data(this PortalType portalType){
      return DATA_MODELS[portalType];
    }

    
    public static Dictionary<PortalType, PortalTypeDataModel> DATA_MODELS = new Dictionary<PortalType, PortalTypeDataModel>(){
      {
        PortalType.Passage,
        new PortalTypeDataModel(
          "Passage",
          NodeType.Portal
        )
      },
      {
        PortalType.OneWayDoor,
        new PortalTypeDataModel(
          "One-Way Door",
          NodeType.Portal
        )
      },
      {
        PortalType.TwoWayDoor,
        new PortalTypeDataModel(
          "Two-Way Door",
          NodeType.Portal
        )
      },
      {
        PortalType.Exit,
        new PortalTypeDataModel(
          "Exit",
          NodeType.Exit
        )
      }
    };
    
  }
  
}