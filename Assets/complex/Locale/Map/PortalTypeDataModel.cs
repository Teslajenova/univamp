namespace Vanitas.Components.Locale.Map{
  public class PortalTypeDataModel{
    
    private string name; // portal type identifier, maybe not be needed
    
    private NodeType defaultNodeType;
    
    // add floor tile texture?
    
    
    public PortalTypeDataModel(string name, NodeType defaultNodeType){
      this.name = name;
      this.defaultNodeType = defaultNodeType;
    }
    
    
    public NodeType NodeType{
      get{
        return defaultNodeType;
      }
    }
    
    
  }
}