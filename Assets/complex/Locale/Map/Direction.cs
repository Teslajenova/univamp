using System.Collections.Generic;

namespace Vanitas.Components.Locale.Map{
  public enum Direction{
    North,
    South,
    East,
    West
  }
  
  static class DirectionExtension{
    
    public static DirectionDataModel Data(this Direction direction){
      return DATA_MODELS[direction];
    }
    
    public static Dictionary<Direction, DirectionDataModel> DATA_MODELS = new Dictionary<Direction, DirectionDataModel>(){
      {
        Direction.North,
        new DirectionDataModel(
          true, false, 
          Direction.South
        )
      },
      {
        Direction.South,
        new DirectionDataModel(
          true, true,
          Direction.North
        )
      },
      {
        Direction.East,
        new DirectionDataModel(
          false, true,
          Direction.West
        )
      },
      {
        Direction.West,
        new DirectionDataModel(
          false, false,
          Direction.East
        )
      }
    };
    
  }
  
}