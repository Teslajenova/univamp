using System.Collections.Generic;
using System.Linq;
using Vanitas.Components.Locale.Map;
using Vanitas.Components.Locale.Util;

namespace Vanitas.Components.Locale{
  public class LocationMap{
    
    private HashSet<Room> rooms = new HashSet<Room>();
    
    private HashSet<Portal> portals = new HashSet<Portal>();
    
    
    public LocationMap(): this(new Room(RoomType.Shop)){
      // generate default map
    }
    
    
    public LocationMap(Room rootRoom){
      rooms.Add(rootRoom); // first room
      // place the main entrance now before getting blocked off
      Portal mainEntrance = new Portal(rootRoom.Type.Data().PortalType);
      
      PortalPlacer.PlaceExternalPortal(mainEntrance, rootRoom, new Grid(rooms, portals));
      
      PlaceSubRooms(rootRoom);
      
      // place area exit
    }
    
    
    private void PlaceSubRooms(Room primeRoom){
      HashSet<Room> subRooms = new HashSet<Room>();
    
      foreach(Room adjoined in primeRoom.Adjoined){
        RoomPlacer.PlaceAdjoined(adjoined, primeRoom, new Grid(this));
        subRooms.Add(adjoined);
      }
      
      foreach(Room adjacent in primeRoom.Adjacent){
        RoomPlacer.PlaceAdjacent(adjacent, primeRoom, new Grid(this));
        subRooms.Add(adjacent);
        
        //Place a portal in between
        PortalPlacer.PlaceConnectingPortal(new Portal(adjacent.Type.Data().PortalType), adjacent, primeRoom, new Grid(this));
      }
      
      foreach(Room subRoom in subRooms){
        PlaceSubRooms(subRoom);
      }
    }
    
    public HashSet<Room> Rooms {
      get {
        return new HashSet<Room>(rooms);
      }
    }

    public HashSet<Portal> Portals {
      get {
        return new HashSet<Portal>(portals);
      }
    }


    /*
    private void PlaceRoom(Room room, Room parentRoom){
      if(rooms.Count == 0 || parentRoom == null){
        // todo: what if a room is not first but has no parent?
        rooms.Add(room);
      }else{
        RoomPlacer.Place
      }
      
    }
    */


    /*
    public LocationMap(){
      // The first room
      AddRoom(5,11, 4,7);
    }
    */
    /*
    public bool AddRoom(int minX, int maxX, int minY, int maxY){
      Room newRoom = new Room(GameCore.RND(minX, maxX+1), GameCore.RND(minY, maxY+1));
      
      return PlaceRoom(newRoom);
    }
    
    private bool PlaceRoom(Room room){
      if(rooms.Count == 0){
        return PlaceTheFirstRoom(room);
      }
      
      //Todo: implement subsequent rooms
      
      return false;
    }
    
    private bool PlaceTheFirstRoom(Room room){
      // when it's the first room just leave it where it is and add a main entrance
       
      if(rooms.Count != 0){
        return false;
      }
      
      rooms.Add(room);
        
      Portal mainEntrance = new Portal(NodeType.MainEntrance);
      
      bool placed = MapLayoutUtil.PlaceMainEntrance(mainEntrance, room, new Grid(rooms, portals));
      
      if(placed){
        portals.Add(mainEntrance);
        return true;
      }
      
      rooms.Remove(room);
      return false;
    }
    */
  }
}