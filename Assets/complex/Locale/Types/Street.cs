using System;
using System.Collections.Generic;

namespace Vanitas.Components.Locale.Types{
  public class Street : LocationTypeDataModel{
    
    public Street(): base("Street", 1.0f, 0.1f, 0.0f){
    }
    
    protected override void InitNameSets(){
      name1st = new HashSet<String>(){
        //"1st",
        //"2nd",
        "3rd",
        "4th",
        "5th",
        "6th",
        "7th",
        "8th",
        "9th",
        "10th",
        "11th",
        "12th",
        "21st",
        "25th",
        "Bohr",
        "Bovein",
        "Bowery",
        "Bradbury",
        "Brea",
        "Byron",
        "Cobbler",
        "Daniels",
        "East",
        "Edward",
        "Fishmonger",
        "Green",
        "Hill",
        "Holloway",
        "Hunter",
        "Jackson",
        "Jameson",
        "King",
        "Kramer",
        "Market",
        "Marlin",
        "Milliner",
        "Mitchell",
        "North",
        "Old",
        "Old Town",
        "Peridot",
        "Polidori",
        "Rustic",
        "Singer",
        "South",
        "Spencer",
        "St. James",
        "Stoker",
        "Thatcher",
        "Thickett",
        "Venn",
        "Vicks",
        "Walnut",
        "Watts",
        "Wedge",
        "West"
      };
      
      name2nd = new HashSet<String>(){
        "Avenue",
        "Boulevard",
        "Drive",
        "Lane",
        "Road",
        "Street",
        "Way"
      };
    }
    
  }
}