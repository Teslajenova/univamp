using System;
using System.Collections.Generic;
using Vanitas.Components.Locale.Map;

namespace Vanitas.Components.Locale.Types{
  public class ServiceStation : LocationTypeDataModel{
    
    public ServiceStation(): base("Service Station"){
    }
    
    
    public virtual LocationMap GenerateMap(){
      
      Room shop = new Room(RoomType.Shop);
      Room diningArea = new Room(RoomType.DiningArea);
      Room kitchen = new Room(RoomType.Kitchen);
      Room restroom1 = new Room(RoomType.Restroom);
      Room restroom2 = new Room(RoomType.Restroom);
      Room storeroom = new Room(RoomType.Storeroom);
      Room staffRoom = new Room(RoomType.StaffRoom);
      
      shop.AddAdjoined(diningArea);
      shop.AddAdjacent(kitchen);
      diningArea.AddAdjacent(restroom1);
      diningArea.AddAdjacent(restroom2);
      kitchen.AddAdjacent(storeroom);
      kitchen.AddAdjacent(staffRoom);
    
      return new LocationMap(shop); // todo: pass in location stand?
    }
    
    
    protected override void InitNameSets(){
            name1st = new HashSet<String>(){
        "Bob's",
        "Donnie's",
        "Eddie's",
        "Gus's",
        "Johnny's",
        "Mike's",
        "Papa's",
        "Pump'n",
      };

            name2nd = new HashSet<String>(){
        "Discount Gas",
        "Gasoline",
        "Oil",
        "Service",
        "Station"
      };
      
    }
    
  }
}