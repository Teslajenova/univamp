using System;
using System.Collections.Generic;

namespace Vanitas.Components.Locale.Types{
  public class Nightclub : LocationTypeDataModel{
    
    public Nightclub(): base("Nightclub"){
      
    }
    
    protected override void InitNameSets(){

            name1st = new HashSet<String>(){
        "Cave",
        "Cell",
        "Corona",
        "Dream",
        "Inferno",
        "Majesty",
        "Mirage",
        "Pit",
        "Savage"
      };
      
      name2nd = new HashSet<String>(name1st);
      
      name2nd.Add("Grotto");
      name2nd.Add("Hole");
      name2nd.Add("House");
      name2nd.Add("Pod");
      
      name1st.Add("Crooked");
      name1st.Add("Heavy");
      name1st.Add("Regal");
      name1st.Add("Sublime");
      
      int duplication = 10;
      
      for(int i = 0; i < duplication; i++){
        name1st.Add("");
        name1st.Add("The");
        name1st.Add("Club");
      }
      
    }
    
  }
}