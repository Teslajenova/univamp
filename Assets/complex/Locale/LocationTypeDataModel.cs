using System;
using System.Collections.Generic;
using System.Linq;
using Vanitas.Components.Attributes;
using Vanitas.Components.Utils;

namespace Vanitas.Components.Locale{
  public abstract class LocationTypeDataModel{
    
    protected HashSet<string> name1st;
    protected HashSet<string> name2nd;
    protected HashSet<string> name3rd;
    
    protected FilterQueue filter1st;
    protected FilterQueue filter2nd;
    protected FilterQueue filter3rd;
    
    protected float percentage1st = 1.0f;
    protected float percentage2nd = 1.0f;
    protected float percentage3rd = 1.0f;
    
    protected String name; // name of the type
    
    protected HashSet<Stand> standRange;
    
    
    // Temp constructor
    //Todo: remove this?
    public LocationTypeDataModel(): this("PLACEHOLDER"){
      //this.name = "PLACEHOLDER";
      //InitNameSets();
      //InitFilters();
    }
    
    
    public LocationTypeDataModel(String name){
      this.name = name;
      InitNameSets();
      InitFilters();
    }
    
    
    //Todo: is this useful?
    public LocationTypeDataModel(String name, float percentage1st, float percentage2nd, float percentage3rd){
      this.name = name;
      this.percentage1st = percentage1st;
      this.percentage2nd = percentage2nd;
      this.percentage3rd = percentage3rd;
      InitNameSets();
      InitFilters();
    }
    
    
    public virtual LocationMap GenerateMap(){
      return new LocationMap(); // todo: pass in location stand?
    }
    
    
    public virtual Stand GenerateStand(){
      if(standRange == null || standRange.Count == 0){
        return Stand.Any;
      }

      List<Stand> standRangeList = standRange.ToList();
      return standRangeList[DataManager.RND(standRange.Count)];
    }
    
    
    public String GenerateName(){
      //Todo: use stringbuilder
      String name = "";
      
      name = AppendedWith(name, filter1st);
      
      name = AppendedWith(name, filter2nd);
      
      name = AppendedWith(name, filter3rd);
      
      name = FinalizeName(name);
      
      return name;
    }
    
    
    protected virtual String FinalizeName(String name){
      return name;
    }
    
    
    protected String AppendedWith(String name, FilterQueue filterQueue){
      if(filterQueue != null){
        name = name.Trim() + " " + filterQueue.Next();
      }
      
      return name;
    }
    
    
    protected void InitFilters(){
      if (name1st != null){
        filter1st = new FilterQueue(name1st, percentage1st);
      }
      
      if (name2nd != null){
        filter2nd = new FilterQueue(name2nd, percentage2nd);
      }
      
      if (name3rd != null){
        filter3rd = new FilterQueue(name3rd, percentage3rd);
      }
    }
    
    
    protected abstract void InitNameSets();
      
  }
}
