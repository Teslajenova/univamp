using Assets.complex.Base;
using System;
using Vanitas.Components.Locale.Map;

namespace Vanitas.Components.Locale.Util{
  public class PlacementOption{
    // todo: rename to RoomPlacementOption
    
    private static readonly int OVERLAP_PENALTY = 10000;
    
    private static readonly int ALIGNED_BONUS = 100;
    
    private static readonly int DISTANCE_PENALTY = 10;
    
    private Point offset;
    private int val;
  
    
    public Point Offset{
      get{
        return offset;
      }
    }
    
    
    public int Value{
      get{
        return val;
      }
    }
    
  
    public PlacementOption(Point offset, bool adjoined, Room room, Room targetRoom, Grid grid){
      this.offset = offset;
      
      CalculateOverlap(room, grid);
      
      int proximity = 2;
      if(adjoined){
        proximity = 1;
      }
      
      CalculateEdgeAlingmentValue(room, targetRoom, proximity);
      
      CalculateDistancePenalty(room, targetRoom);
      
      // Todo: add corner alignment bonus
      
    }
    
    
    private void CalculateDistancePenalty(Room room, Room targetRoom){
      
      // get shortest distance between room corners
      int bestDistance = 999999;
      
      foreach(Node rNode in room.GetNodes(NodeType.Corner)){
        foreach(Node trNode in targetRoom.GetNodes(NodeType.Corner)){
          
          int dx = Math.Abs((rNode.Coordinates.X + offset.X) - trNode.Coordinates.X);
          
          int dy = Math.Abs((rNode.Coordinates.Y + offset.Y) - trNode.Coordinates.Y);
          
          int dist = dx + dy;
          
          if(dist < bestDistance){
            bestDistance = dist;
          }
          
        }
      }
      
      val -= bestDistance * DISTANCE_PENALTY;
    }
    
    
    private Point OffsetPoint(Point original){
      return new Point(original.X + offset.X, original.Y + offset.Y);
    }
    
    
    private void CalculateOverlap(Room room, Grid grid){
      foreach(Node roomNode in room.Nodes){
        Point offsetPoint = OffsetPoint(roomNode.Coordinates);
        
        if(grid.GetNodeAt(offsetPoint) != null){
          val -= OVERLAP_PENALTY;
        }
      }
      
    }
    
    
    private void CalculateEdgeAlingmentValue(Room room, Room targetRoom, int proximity){
      foreach(Node node in room.GetBoundaryNodes()){
        Point offsetPoint = OffsetPoint(node.Coordinates);
        
        foreach(Node targetNode in targetRoom.GetBoundaryNodes()){
          if(Proximity(offsetPoint, targetNode.Coordinates, proximity)){
            val += ALIGNED_BONUS;
          }
        }
      }
    }
  
    
    private bool Proximity(Point p1, Point p2, int prox){
      if(p1.X == p2.X){
        if(p1.Y == p2.Y + prox ||
           p1.Y == p2.Y - prox){
          return true;
        }
      }else if(p1.Y == p2.Y){
        if(p1.X == p2.X + prox ||
           p1.X == p2.X - prox){
          return true;
        }
      }
      
      return false;
    }
    
    
  }
}