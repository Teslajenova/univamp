using Assets.complex.Base;
using System.Collections.Generic;
using Vanitas.Components.Locale.Map;
using System.Linq;

namespace Vanitas.Components.Locale.Util{
  public class RoomPlacer{
  
    private static readonly int FINAL_COUNT = 5;
    
    
    public static bool PlaceAdjoined(Room newRoom, Room targetRoom, Grid grid){
      return Place(true, newRoom, targetRoom, grid);
    }
    
    
    public static bool PlaceAdjacent(Room newRoom, Room targetRoom, Grid grid){
      return Place(false, newRoom, targetRoom, grid);
    }
    
    
    public static bool Place(bool adjoined, Room newRoom, Room targetRoom, Grid grid){ 
      
      List<PlacementOption> closedSet = new List<PlacementOption>();
      
      List<PlacementOption> fringe = new List<PlacementOption>();
      
      fringe.AddRange(GetInitialOptions(adjoined, newRoom, targetRoom, grid));
      
      //fringe.Add(new PlacementOption(new Point(0,0), adjoined, newRoom, targetRoom, grid));
      
      PlacementOption bestOption = fringe[0]; // is this assignment necessary?
      int counter = 0;
      
      do{
        // get most valuable fringe option
        fringe = (List<PlacementOption>)(fringe.OrderByDescending(x => x.Value));
        PlacementOption bestOfFringe = fringe[0];
        
        // move it to closed set
        fringe.Remove(bestOfFringe);
        closedSet.Add(bestOfFringe);
        
        // Get nearby fringe options from that (that are not yet in closed set or fringe)
        List<Point> newPoints = GetNewPoints(bestOfFringe, closedSet, fringe);
      
        // Put new fringe options to fringe
        foreach(Point point in newPoints){
          fringe.Add(new PlacementOption(point, adjoined, newRoom, targetRoom, grid));
        }
        
        // check most valuable option in closed set
        closedSet = (List<PlacementOption>)(closedSet.OrderByDescending(x => x.Value));
        
        if (bestOption == closedSet[0]){
          counter++;
        }else{
          bestOption = closedSet[0];
          counter = 0;
        }
        
      }while(counter < FINAL_COUNT);
      
      newRoom.Move(bestOption.Offset);
      return bestOption.Value > 0;
    }
    
    
    private static List<PlacementOption> GetInitialOptions(bool adjoined, Room room, Room targetRoom, Grid grid){
      List<PlacementOption> options = new List<PlacementOption>();
      
      foreach(Node node in targetRoom.GetNodes(NodeType.Corner)){
        options.Add(new PlacementOption(node.Coordinates, adjoined, room, targetRoom, grid));
      }
      
      return options;
    }
    
    
    private static List<Point> GetNewPoints(PlacementOption option, List<PlacementOption> closedSet, List<PlacementOption> fringe){
      
      List<Point> nearbyPoints = GetNearbyPoints(option.Offset);
      
      // filter out coordinates we've seen before
      
      for(int i = 0; i < nearbyPoints.Count; i++){
        Point point = nearbyPoints[i];
        
        foreach(PlacementOption closedOption in closedSet){
          if(point.Equals(closedOption.Offset)){
            nearbyPoints.Remove(point);
          }
        }
        
        foreach(PlacementOption fringeOption in fringe){
          if(point.Equals(fringeOption.Offset)){
            nearbyPoints.Remove(point);
          }
        }
      }
      
      return nearbyPoints;
    }
    
    
    private static List<Point> GetNearbyPoints(Point p){
      List<Point> points = new List<Point>();
      
      // Todo: add more?
      points.Add(new Point(p.X +1, p.Y));
      points.Add(new Point(p.X -1, p.Y));
      points.Add(new Point(p.X, p.Y +1));
      points.Add(new Point(p.X, p.Y -1));
      
      return points;
    }
    
    
  }
}