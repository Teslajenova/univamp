using Assets.complex.Base;
using System;
using System.Collections.Generic;
using Vanitas.Components.Locale.Map;

namespace Vanitas.Components.Locale.Util{
  public class PortalPlacementOption{
    
    private static readonly int OVERLAP_PENALTY = 100000;
    private static readonly int EDGE_DISTANCE_PENALTY = 1000;
    private static readonly int ORIGIN_DISTANCE_PENALTY = 10;
    
    
    private Point offset;
    private int val = (int)(1.5f * EDGE_DISTANCE_PENALTY); // some starting points because the ideal edge distance will result in 1 * the penalty
    
    
    public Point Offset{
      get{
        return offset;
      }
    }
    
    
    public int Value{
      get{
        return val;
      }
    }
    
    
    public PortalPlacementOption(Point offset, Portal portal, Room primaryRoom, Point origin, Room secondaryRoom, Grid grid){
      this.offset = offset;
      
      CheckOverlap(grid);
      
      CheckDistanceToEdge(primaryRoom.GetNodes(NodeType.Edge));
                          
      CheckDistanceToOrigin(origin);
      
      // connecting portal
      if(secondaryRoom != null){
        
        
        // check edge distance to the other room
        CheckDistanceToEdge(primaryRoom.GetNodes(NodeType.Edge));
      }
    }
    
                          
    private void CheckDistanceToOrigin(Point origin){
      int dx = Math.Abs(origin.X - offset.X);
      int dy = Math.Abs(origin.Y - offset.Y);
      
      val -= ORIGIN_DISTANCE_PENALTY * (dx + dy);
    }
    
                        
    private void CheckDistanceToEdge(HashSet<Node> edgeNodes){
      // get nearest edge node distance
      int bestDist = 999999;
      
      foreach(Node node in edgeNodes){
        int dx = Math.Abs(node.Coordinates.X - offset.X);
        int dy = Math.Abs(node.Coordinates.Y - offset.Y);
        
        int dist = dx + dy;
        
        if(dist < bestDist){
          bestDist = dist;
        }
        
      }
      
      val -= bestDist * EDGE_DISTANCE_PENALTY;
    }
                          
    
    private void CheckOverlap(Grid grid){
      if(grid.GetNodeAt(offset) != null){
        val -= OVERLAP_PENALTY;
      }
    }
    
  }
}