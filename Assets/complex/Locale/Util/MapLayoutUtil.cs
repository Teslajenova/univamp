
using Assets.complex.Base;
using System.Collections.Generic;
using Vanitas.Components.Locale.Map;
using Vanitas.Components.Utils;

namespace Vanitas.Components.Locale.Util{
  public class MapLayoutUtil{

    private static List<Direction> mainEntranceDirPref = new List<Direction>(){
      Direction.West,
      Direction.North,
      Direction.South,
      Direction.East
    };
      
    public static bool PlaceMainEntrance(Portal entrance, Room room, Grid grid){
      
      foreach(Direction direction in mainEntranceDirPref){
        if(PlaceMainEntrance(entrance, room, direction, grid)){
          return true;
        }
      }
      
      return false;
      
      //Direction direction = Direction.West;
      
      //return PlaceMainEntrance(entrance, room, direction, grid);
      
    }
      
    public static bool PlaceMainEntrance(Portal entrance, Room room, Direction direction, Grid grid){
      
      HashSet<Node> bNodes = room.GetBoundaryNodes(direction);
      
      Queue<Node> rNodes = Shuffle<Node>.This(bNodes);
      
      foreach(Node node in rNodes){
        
        if(grid.GetNodeFrom(node, direction, 1) != null){
          continue;
        }
        
        Point candidatePoint = direction.Data().Offset(node.Coordinates, 1);
        
        HashSet<Node> obstacles = grid.GetSurroundingNodes(candidatePoint, direction.Data().Opposite(), false);
        
        if(obstacles.Count == 0){
          entrance.PlaceAt(candidatePoint);
          return true;
        }
      }
      
      return false;
    }
    
  }
}