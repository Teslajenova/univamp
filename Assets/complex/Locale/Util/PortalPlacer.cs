using Assets.complex.Base;
using System.Collections.Generic;
using Vanitas.Components.Locale.Map;
using System.Linq;

namespace Vanitas.Components.Locale.Util{
  public class PortalPlacer{
  
    // todo: combine this with RoomPlacer
    
    private static readonly int FINAL_COUNT = 5;
    
    
    public static bool PlaceExternalPortal(Portal portal, Room room, Grid grid){
      return Place(portal, room, null, grid);
    }
    
    
    public static bool PlaceConnectingPortal(Portal portal, Room primaryRoom, Room secondaryRoom, Grid grid){ 
      return Place(portal, primaryRoom, secondaryRoom, grid);
    }
    
    
    private static bool Place(Portal portal, Room primaryRoom, Room secondaryRoom, Grid grid){
      
      List<PortalPlacementOption> closedSet = new List<PortalPlacementOption>();
      
      List<PortalPlacementOption> fringe = new List<PortalPlacementOption>();
      
      Point roomOrigin = primaryRoom.GetRandomNode().Coordinates;
      
      fringe.Add(new PortalPlacementOption(new Point(roomOrigin.X, roomOrigin.Y), portal, primaryRoom, roomOrigin, secondaryRoom, grid));
      
      PortalPlacementOption bestOption = null; // assign first of fringe here?     
      
      int counter = 0;           
      
      do{
        
        // get most valuable fringe option
        fringe = (List<PortalPlacementOption>)(fringe.OrderByDescending(x => x.Value));
        PortalPlacementOption bestOfFringe = fringe[0];
        
        // move it to closed set
        fringe.Remove(bestOfFringe);
        closedSet.Add(bestOfFringe);
        
        // Get nearby fringe options from that (that are not yet in closed set or fringe)
        List<Point> newPoints = GetNewPoints(bestOfFringe, closedSet, fringe);
        
        // Put new fringe options to fringe
        foreach(Point point in newPoints){
          fringe.Add(new PortalPlacementOption(point, portal, primaryRoom, roomOrigin, secondaryRoom, grid));
        }
        
        // check most valuable option in closed set
        closedSet = (List<PortalPlacementOption>)(closedSet.OrderByDescending(x => x.Value));
        
        if (bestOption == closedSet[0]){
          counter++;
        }else{
          bestOption = closedSet[0];
          counter = 0;
        }
        
      }while(counter < FINAL_COUNT);
      
      portal.PlaceAt(bestOption.Offset);
      return bestOption.Value > 0;
    }
    
    
    private static List<Point> GetNewPoints(PortalPlacementOption option, List<PortalPlacementOption> closedSet, List<PortalPlacementOption> fringe){
      
      List<Point> nearbyPoints = GetNearbyPoints(option.Offset);
      
      // filter out coordinates we've seen before
      
      for(int i = 0; i < nearbyPoints.Count; i++){
        Point point = nearbyPoints[i];
        
        foreach(PortalPlacementOption closedOption in closedSet){
          if(point.Equals(closedOption.Offset)){
            nearbyPoints.Remove(point);
          }
        }
        
        foreach(PortalPlacementOption fringeOption in fringe){
          if(point.Equals(fringeOption.Offset)){
            nearbyPoints.Remove(point);
          }
        }
      }
      
      return nearbyPoints;
    }
    
    
    private static List<Point> GetNearbyPoints(Point p){
      List<Point> points = new List<Point>();
      
      // Todo: add more?
      points.Add(new Point(p.X +1, p.Y));
      points.Add(new Point(p.X -1, p.Y));
      points.Add(new Point(p.X, p.Y +1));
      points.Add(new Point(p.X, p.Y -1));
      
      return points;
    }
  
  }  
}