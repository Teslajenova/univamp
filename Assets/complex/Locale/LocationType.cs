using System.Collections.Generic;
using Vanitas.Components.Locale.Types;

namespace Vanitas.Components.Locale{
  public enum LocationType{
    //Apartment,
    Nightclub,
    //Park,
    ServiceStation,
    Street
  }
  
  static class LocationTypeExtension{
    
    public static LocationTypeDataModel Data(this LocationType type){
      return DATA_MODELS[type];
    }
    
    
    public static Dictionary<LocationType, LocationTypeDataModel> DATA_MODELS = new Dictionary<LocationType, LocationTypeDataModel>(){
      /*{
        LocationType.Apartment,
        new Apartment()
      },*/
      {
        LocationType.Nightclub,
        new Nightclub()
      },
      {
        LocationType.ServiceStation,
        new ServiceStation()
      },
      {
        LocationType.Street,
        new Street()
      }
    };
    
  }
}