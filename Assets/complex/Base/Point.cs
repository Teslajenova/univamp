﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.complex.Base {
  public struct Point {

    public int X, Y;

    public Point(int px, int py) {
      X = px;
      Y = py;
    }

    
    public Boolean Equals(Point otherPoint) {
      if (this.X == otherPoint.X && this.Y == otherPoint.Y) {
        return true;
      }

      return false;
    }
    

  }
}
