//using EmptyKeys.UserInterface.Mvvm;
using System;
using System.Collections.Generic;
using System.Text;
using Vanitas.Components.Ambition;
using Vanitas.Components.Attributes;
using Vanitas.Components.Commodity;
using Vanitas.Components.Linkage;
using Vanitas.Components.Property;
using Vanitas.Components.Utils;

namespace Vanitas.Components{
  public class Character{ //: BindableBase {
    
    private bool defined = false;
    
    private CharacterType characterType;
    private Dictionary<Stat, int> stats = new Dictionary<Stat, int>();
    private HashSet<Trait> traits;
    
    private String name;
    private Clan? clan = null;
    private int? generation = null;
    private Relations relations;
    private Possessions possessions;
    private ErgSpectrum ergSpectrum;
    
    
    public Character(CharacterType characterType, HashSet<Trait> traits){
      
      this.characterType = characterType;
      this.traits = traits;
      
      foreach(StatRange statRange in characterType.Data().StatRanges()){
        stats.Add(statRange.Stat, statRange.GetValue());
      }
      
      if(characterType.Data().Sex == Sex.Female){
        name = FemaleNames.Random();
      } else {
        name = MaleNames.Random();
      }
      
      this.relations = new Relations(this);
      
      this.ergSpectrum = new ErgSpectrum();
      
      this.possessions = new Possessions(this);
    }
    
    
    public int GetStat(Stat stat){
      int val = 0;
      stats.TryGetValue(stat, out val);
      
      foreach(Trait trait in traits){
        val += trait.Data().Stat(stat);
      }
      
      return val;
    }
    
    
    // Maybe this should be in clan class
    public float EvaluateBy(Clan clan){
      return MembershipEvaluation.By(clan, this);
    }
    
    
    public bool IsVampire(){
      return this.clan != null;
    }
    
    
    public bool SetClanAndGen(Clan clan, int generation){
      if(IsVampire() ||
        generation > Config.DEFAULT_GENERATION ||
        generation < Config.TERMINAL_GENERATION){
        return false;
      }
      
      this.clan = clan;
      this.generation = generation;
      return true;
    }
    
    
    public void ModifyStat(Stat stat, int modValue){
      int currentStat = GetStat(stat);
      stats[stat] = currentStat + modValue;
    }
    
    
    public bool ElevateGeneration(){
      if(generation > Config.TERMINAL_GENERATION){
        generation -= 1;
        return true;
      }
      
      return false;
    }
    
    
    public bool Defined{
      get{
        return this.defined;
      }
    }
    
    
    public void Define(){
      if(defined){
        return;
      }
      
      int acquaintances = GetStat(Stat.Social) + DataManager.RND(Config.RANDOM_EXTRA_ACQUAINTANCES +1);
      
      for(int i = 0; i < acquaintances; i++){
        //TODO: implement this
        //RelationsGenerator.GeneraterAcquaintanceFor(this);
      }

      BackgroundUtil.DefineBackgrounds(this);
      
      defined = true;
    }
    
    
    public Clan? Clan{
      get{
        return this.clan;
      }
    }

    
    public int? Generation{
      get{
        return this.generation;
      }
    }
    
    
    public string Name{
      get { 
        return this.name; 
      }
    }
    
    
    public string Outline(){
      StringBuilder sb = new StringBuilder(name);
      
      sb.Append(" - The ");
      
      //for(int i = 0; i < traits.Size; i++){
        //sb.Append(traits.);
      //}
      sb.Append(characterType.Data().Name);
      
      return sb.ToString();
    }
    
    
    public Stand Stand{
      get{
        return this.characterType.Data().Stand;
      }
    }
    
    
    public Relations Relations{
      get{
        return this.relations;
      }
    }

    
    public Possessions Possessions {
      get {
        return this.possessions;
      }
    }
    
    
    public ErgSpectrum ErgSpectrum{
      get{
        return this.ergSpectrum;
      }
    }
    
    
    public HashSet<Vendible> Vendibles{
      get{
        return this.characterType.Data().Vendibles;
      }
    }

    
    // Wtf is this?
    //public object RelationsGenerator { get; private set; }

    
    public HashSet<Need> MatchedNeeds(){
      return this.characterType.Data().MatchedNeeds();
    }
    
    
    public bool Provides(Need need){
      foreach(Need matchedNeed in MatchedNeeds()){
        if(need.Contains(matchedNeed)){
          return true;
        }
      }
      
      return false;
    }
    
  }
}