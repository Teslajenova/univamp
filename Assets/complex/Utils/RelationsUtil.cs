using System.Collections.Generic;
using System.Linq;
using Vanitas.Components.Attributes;
using Vanitas.Components.Linkage;
using Vanitas.Components.Linkage.Links;

namespace Vanitas.Components.Utils{
  public class RelationsUtil{
    
    public static void Acquaint(Character c1, Character c2){
      c1.Relations.Add(new Connection(ConnectionType.Acquaintance, c2));
      c2.Relations.Add(new Connection(ConnectionType.Acquaintance, c1));
    }
    
    
    public static void Befriend(Character c1, Character c2){
      c1.Relations.Add(new Connection(ConnectionType.Ally, c2));
      c2.Relations.Add(new Connection(ConnectionType.Ally, c1));
      
      UpdateBackground(c1, Stat.Allies);
      UpdateBackground(c2, Stat.Allies);
    }
    
    
    public static void Associate(Character emptor, Character contact){
      contact.Relations.Add(new Connection(ConnectionType.Emptor, emptor));
      
      emptor.Relations.Add(new Connection(ConnectionType.Contact, contact));
      
      UpdateBackground(emptor, Stat.Contacts);
    }
    
    
    private static void UpdateBackground(Character character, Stat stat){
      if(!character.Defined){
        return;
      }
      
      //increase
      while(stat.Data()
            .CurrentCount(character)
            >= stat.Data()
            .UnitCountForValue( character.GetStat(stat) +1)){
        character.ModifyStat(stat, 1);
      }
      
      //Decrease
      while(stat.Data()
            .CurrentCount(character)
            < stat.Data()
            .UnitCountForValue( character.GetStat(stat))){
        character.ModifyStat(stat, -1);
      }
    }
    
  }
}