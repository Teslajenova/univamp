using System.Collections.Generic;
using System;
using System.Linq;

namespace Vanitas.Components.Utils{
  public class Shuffle<T>{
    
    public static Queue<T> This(HashSet<T> hashSet){
      Queue<T> queue = new Queue<T>();
      
      List<T> list = hashSet.ToList();
      Slam(list);
      
      foreach(T item in list){
        queue.Enqueue(item);
      }
      
      return queue;
    }
    
    public static void Slam(IList<T> list){ 
      int n = list.Count; 
      while (n > 1) { 
        n--; 
        int k = DataManager.RND(n + 1); 
        T val = list[k]; 
        list[k] = list[n]; 
        list[n] = val; 
      } 
    }

  }
}