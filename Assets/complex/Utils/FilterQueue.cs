using System;
using System.Collections.Generic;
using System.Linq;

namespace Vanitas.Components.Utils{
  public class FilterQueue{
    
    private Queue<String> queue = new Queue<String>();
    
    private HashSet<String> masterSet;
    
    private float percentage;
    
    
    public FilterQueue(HashSet<String> masterSet, float percentage){
      this.masterSet = masterSet;
      this.percentage = percentage;
    }
    
    
    public String Next(){
      if(queue.Count <= 0){
        FillQueue();
      }
      
      return queue.Dequeue();
    }
    
    
    private void FillQueue(){
      List<String> tempList = masterSet.ToList();
      
      Shuffle(tempList);
      
      int count = (int)Math.Round(tempList.Count * percentage);
      
      if(count < 1){
        count = 1;
      }
      
      for(int i = 0; i < count; i++){
        queue.Enqueue(tempList[i]);
      }
    }
    
    
    private void Shuffle(List<String> list) { 
      int n = list.Count; 
      while (n > 1) { 
        n--; 
        int k = DataManager.RND(n + 1); 
        String val = list[k]; 
        list[k] = list[n]; 
        list[n] = val; 
      } 
    }
    
  }
}