

using System.Collections.Generic;
using System.Linq;
using Vanitas.Components.Ambition;
using Vanitas.Components.Attributes;

namespace Vanitas.Components.Utils{
  public class CharacterGenerator{
    
    //public static object GameState { get; private set; } // todo: ????

    public static Character NewCitizen(Stand stand, Stat stat){
      Character character;
      
      do{
        character = NewCitizen(stand);
      }while(character
             .GetStat(stat) <= 0);
      
      return character;
    }
    
    public static Character NewCitizen(Stand stand, Need need){
      Character character;
      
      do{
        character = NewCitizen(stand);
      }while(!character.Provides(need));
      
      return character;
    }
    
    public static Character NewCitizen(Stand stand){
      CharacterType type = CharacterTypeExtension.Random(stand);
      //GetRandomType(stand); //CharacterType.Random();

      int numberOfTraits = DataManager.RND(Config.MAX_INITIAL_TRAITS +1);

      HashSet<Trait> traits = new HashSet<Trait>();

      for(int i = 0; i < numberOfTraits; i++){
        Trait trait;

        do {
          trait = TraitExtension.Random(type.Data().Sex);
        }while(TraitIsDuplicate(trait, traits));

        traits.Add(trait);
      }
      
      return new Character(type, traits);
    }
    
    private static bool TraitIsDuplicate(Trait trait, HashSet<Trait> prevTraits){
      
      foreach(Trait prevTrait in prevTraits){
        if(trait == prevTrait){
          return true;
        }
      }
      
      return false;
    }
    
    public static Character NewVampire(Clan clan) {
      return NewVampire(clan, Data.INSTANCE.City);
    }

    public static Character NewVampire(Clan clan, City city){
      Character sire = ExistingVampire(clan, city);
        
      List<Character> clanPreference = city.GetMortals().OrderByDescending(c => c.EvaluateBy(clan)).ToList(); 
        
      Character fledgeling = clanPreference[0];
      
      bool success = false;
      
      if(sire == null){
        success = Transform.MortalToVampire(fledgeling, clan, Config.DEFAULT_GENERATION);
      }else{
        success = Transform.MortalToVampire(fledgeling, sire);
      }
      
      if(success){
        return fledgeling;
      }
      
      return null;
    }
    
    public static Character ExistingVampire(Clan clan) {
      return ExistingVampire(clan, Data.INSTANCE.City);
    }

    public static Character ExistingVampire(Clan clan, City city){
      Character vampire = null;

      List<Character> clanMembers = city.GetClanMembers(clan).ToList(); 
      
      if(clanMembers.Count > 0){
        vampire = clanMembers[DataManager.RND(clanMembers.Count)];
      }
      
      return vampire;
    }
    
    public static Character GetOrCreateVampire(Clan clan){
      Character vampire = ExistingVampire(clan);
      
      if(vampire == null){
        vampire = NewVampire(clan);
      }
      
      return vampire;
    }
    
    /*
    private static CharacterType GetRandomType(Stand stand){
      CharacterType type;
      
      do{
        type = CharacterTypeExtension.Random();
      }while(!type.Data().Stand.Equivalent(stand));
      
      return type;
    }
    */
    
  }
}