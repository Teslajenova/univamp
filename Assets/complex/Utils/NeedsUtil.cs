using System.Collections.Generic;
using Vanitas.Components.Ambition;
using System.Linq;

namespace Vanitas.Components.Utils{
  public class NeedsUtil{


    //Todo: maybe move this to Need.cs
    private static HashSet<Need> ALL_NEEDS = new HashSet<Need>(){
      Need.Existence,
      Need.Relatedness,
      Need.Growth
    };
    
    
    public static Need RandomNeed(){
      return ALL_NEEDS.ToList()[DataManager.RND(ALL_NEEDS.Count)];
    }
    
    /*
    public static Need CorrectedNeed(Need need, Character character){
      if(character.IsVampire()){
        if(need == Need.Existence){
          return Need.VampiricExistence;
        }
      }
      
      return need;
    }
    */
    
  }
}