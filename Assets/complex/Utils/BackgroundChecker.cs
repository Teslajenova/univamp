using Vanitas.Components.Attributes;
using Vanitas.Components.Linkage.Links;

namespace Vanitas.Components.Utils{
  public class BackgroundChecker{
    
    
    // Todo: maybe this should be in StatDataModel
    public static bool BackgroundIsPrimed(Stat stat, Character character){
      if(character.Defined){
        return true;
      }
      
      int statValue = character.GetStat(stat);
      
      if(statValue <= 0){
        return true;
      }
      
      int? amountRequired = stat.Data().UnitCountForValue(statValue);
      
      if(amountRequired == null){
        return true;
      }
      
      int currentAmount = (int)stat.Data().CurrentCount(character);
      //todo: console this. what does null turn into?
        //GetCurrentUnitAmount(stat, character);
      
      return currentAmount >= amountRequired;
    }
    
    /*
    public static int GetCurrentUnitAmount(Stat stat, Character character){
      
      if(stat == Stat.Allies){
        return character.Relations
          .GetEach<Friend>().Count;
      }else if(stat == Stat.Contacts){
        return character.Relations
          .GetEach<Contact>().Count;
      }
      //todo: add fame, influence(?), resources...
      return 0;
    }
    */
   
    /*
    public static bool AlliesArePrimed(Character character){
      if(character.Defined){
        return true;
      }
      
      int alliesStat = character.Stat(Stat.Allies):
      
      if(alliesStat == 0){
        return true;
      }
      
      int friendsRequired = 
        Stat.Allies.Data()
        .UnitCountForValue(alliesStat);
      
      int currentFriends = 
        character.Relations
        .GetEach<Friend>().Count;
      
      return currentFriends >= friendsRequired;
    }
    */
    
  }
}