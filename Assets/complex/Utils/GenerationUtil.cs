using System.Collections.Generic;

namespace Vanitas.Components.Utils{
  public class GenerationUtil{
    
    public static bool ElevateBloodlineGen(Character character){
      if(!character.IsVampire()){
        return false;
      }
      
      //List<Character> ancestry = character.Relations.GetAncestry();
      
      List<Character> linearAncestry = GetLinearAncestry(character);
      
      if(linearAncestry.Count > 0){
        Character progenitor = linearAncestry[linearAncestry.Count -1];
      
        if(progenitor.Generation <= Config.TERMINAL_GENERATION){
        return false;
        }
      }
      
      // Becomes 12 (def -1)
      character.ElevateGeneration();
      
      //int comparativeGen = character.Generation;
      
      foreach(Character ancestor in linearAncestry){
        ancestor.ElevateGeneration();
      }
      
      return true;
    }
    
    private static List<Character> GetLinearAncestry(Character character){
      List<Character> la = new List<Character>();
      
      if(!character.IsVampire()){
        return la;
      }
      
      List<Character> ancestry = character.Relations.GetAncestry();
      
      int linearGen = (int)character.Generation;
      
      foreach(Character ancestor in ancestry){
        linearGen--;
        if(ancestor.Generation != linearGen){
          break;
        }
        la.Add(ancestor);
      }
      
      return la;
    }
    
  }
}