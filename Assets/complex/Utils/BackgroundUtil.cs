using Vanitas.Components.Attributes;

namespace Vanitas.Components.Utils{
  public class BackgroundUtil{
    
    public static void DefineBackgrounds(Character character){
      //allies
      DefineBG(Stat.Allies, character);
      //Contacts
      DefineBG(Stat.Contacts, character);
      //Fame
      //Influence???
      //Retainers???
      
      //Resources
      
    }
    
    private static void DefineBG(Stat stat, Character character){
      int statValue = character.GetStat(stat);
      
      if(statValue <= 0){
        return;
      }
      
      int? targetAmount = stat.Data().RandomValidCountForValue(statValue);
      
      if(targetAmount == null || targetAmount <= 0){
        return;
      }
      while(stat.Data().CurrentCount(character) < targetAmount){
      
      //while(!stat.IsPrimed(character)){
        if(stat == Stat.Allies){
          RelationsGenerator
            .GenerateAllyFor(character);
        }else if(stat == Stat.Contacts){
          RelationsGenerator
            .GenerateContactFor(character);
        
        //Todo: add fame, influence(?), reaources...
        }else{
          return;
        }

      }
      
    }
    
    /*
    public static void DefineAllies(Character character){
      int alliesStat = character.Stat(Stat.Allies);
      
      if(alliesStat == 0){
        return;
      }
      
      int amount = Stat.Allies.Data().RandomValidCountForValue(alliesStat);
      
      while(character.Relations.GetEach<Friend>().Count < amount){
        AddNewFriend(character);
      }
    }
    */
    
    /*
    private static void AddNewFriend(Character character){
      Stand stand = character.Stand;
      
      if(GameCore.RND(GameConfig.ALLY_STAND_RANDOMIZER) == 0){
        stand == Stand.Any;
      }
      
      List<Character> existingFriends = character.Relations.GetEach<Friend>();
      
      List<Character> missingAllies = CharacterPicker.ThoseMissingAllies();
        
      List<Character> potentialFriends 
          = missingAllies
          .Where(x => x.Stand.Equivalent(stand) && !existingFriends.Contains(x));
        // may include vampires...
        
      Character newFriend;
        
      if(potentialFriends.Count <= 0){
        newFriend = CharacterGenerator
            .NewCitizen(stand, 
                        Stat.Allies);
      }else{
        newFriend = potentialAllies[potentialAllies.Count -1];
      }
      
      EngineState.CITY.AddCharacter(newFriend);
      
      newFriend.Relations.Add(new Acquaintance(character));
      newFriend.Relations.Add(new Friend(character));
      
      character.Relations.Add(new Acquaintance(newFriend));
      character.Relations.Add(new Friend(newFriend));
    }
    */
 
    /*
    public static void DefineContacts(Character character){
      int contactsStat = character.Stat(Stat.Contacts);
      
      if(contactsStat == 0){
        return;
      }
      
      int amount = Stat.Contacts.Data().RandomValidCountForValue(contactsStat);
      
    }
    */
    
  }
}