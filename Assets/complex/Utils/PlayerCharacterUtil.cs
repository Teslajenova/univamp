using Vanitas.Components.Attributes;

namespace Vanitas.Components.Utils{
  public class PlayerCharacterUtil{
    
    public static bool CharacterSetup(Character character, Clan clan, int index){
      Character sire = CharacterGenerator.GetOrCreateVampire(clan);
      
      bool success = Transform.MortalToVampire(character, sire);
      
      if(!success){
        return false;
      }
      
      character.Define();
      Data.INSTANCE.City.SetProtagonist(character, index);
      
      return true;
    }
    
    
  }
}