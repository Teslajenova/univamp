using System.Collections.Generic;
using Vanitas.Components.Attributes;

namespace Vanitas.Components.Utils{
  public class CharacterPicker{
    
    public static List<Character> ThoseMissingAllies(){
      List<Character> those = new List<Character>();
      
      foreach(Character c in Data.INSTANCE.City.Characters){
        //todo: check this stuff
        if(!BackgroundChecker
           .BackgroundIsPrimed(Stat.Allies, c)){
          those.Add(c);
        }
      }
      
      return those;
    }
    
  }
}