using Vanitas.Components.Attributes;
using Vanitas.Components.Linkage;
using Vanitas.Components.Linkage.Links;

namespace Vanitas.Components.Utils{
  public class Transform{
   
    
    public static bool MortalToVampire(Character character, Character sire){
      
      if(sire.Clan == null) {
        return false;
      }

      Clan siresClan = (Clan)sire.Clan;
      int siresGeneration = (int)sire.Generation;
      
      int generation = 0;
      
      if(siresGeneration < Config.DEFAULT_GENERATION){
        generation = siresGeneration +1;
      }else if(siresGeneration == Config.DEFAULT_GENERATION){
        if(GenerationUtil.ElevateBloodlineGen(sire)){
          generation = Config.DEFAULT_GENERATION;
        }
      }
      
      if(generation == 0){
        return false;
      }
      
      if(!MortalToVampire(character, siresClan, generation)){
        return false;
      }
      
      sire.Relations.Add(new Childe(character));
      sire.Relations.Add(new Thrall(character));
      sire.Relations.Add(new Acquaintance(character));
      
      character.Relations.Add(new Sire(sire));
      //character.Relations.Add(new BloodBond(sire, 1)); // todo: fix blood bond -> connection
      character.Relations.Add(new Acquaintance(sire));

      return true;
    }

    public static bool MortalToVampire(Character character, Clan clan, int generation){
      if (character.IsVampire()){
        return false;
      }
      
      bool allSet = character.SetClanAndGen(clan, generation);
      
      if (!allSet){
        return false;
      }
      
      return true;
    }
    
  }
}