using System.Collections.Generic;
using System.Linq;
using Vanitas.Components.Ambition;
using Vanitas.Components.Attributes;
using Vanitas.Components.Linkage;

namespace Vanitas.Components.Utils{
  public class RelationsGenerator{
    
    public static void GenerateAcquaintanceFor(Character character){
      
      Stand stand = GetSemiRandomStand(character, Config.ACQUAINTANCE_STAND_RANDOMIZER);
      
      bool entirelyNew = DataManager.RND(Config.NEW_VS_OLD_CHARACTER_RANDOMIZER) == 0;
      
      Character acquaintance = null;
      
      if(!entirelyNew){
        List<Character> potentials = GetPotentialAcquaintances(character, stand);
        
        if(potentials.Count > 0){
          acquaintance = potentials[DataManager.RND(potentials.Count)];
        }
      }
      
      if(acquaintance == null){
        acquaintance = CharacterGenerator.NewCitizen(stand);
      }
      
      RelationsUtil.Acquaint(character, acquaintance);
    }
    
    
    public static List<Character> GetPotentialAcquaintances(Character character, Stand stand){
      List<Character> alreadyKnown = character.Relations.GetAllCharacters();
      
      List<Character> available = Data.INSTANCE.City.GetCharacters();
      
      return available.Where(x => x != character && !alreadyKnown.Contains(x)).ToList();
    }
    
    
    public static void GenerateContactFor(Character character){
      Need need = 
        NextContactNeed(character)
        .CorrectedFor(character);
      
      // get existing character
      
      Character newContact = 
        CharacterGenerator
        .NewCitizen(Stand.Any, need);
      
      RelationsUtil.Acquaint(character, newContact);
      RelationsUtil.Associate(character, newContact);
    }
    
    
    private static Need NextContactNeed(Character character){
      if(DataManager.RND(Config.NEXT_NEED_RANDOMIZER) == 0){
        return NeedsUtil.RandomNeed();
      }
      
      List<Need> unmatchedNeeds = new List<Need>();
      
      unmatchedNeeds.AddRange(NeedList(
        Need.Existence, 
        character));
      
      unmatchedNeeds.AddRange(NeedList(
        Need.Relatedness,
        character));
      
      unmatchedNeeds.AddRange(NeedList(
        Need.Growth,
        character));
      
      if(unmatchedNeeds.Count > 0){
        return unmatchedNeeds[DataManager.RND(unmatchedNeeds.Count)];
      }
      
      return NeedsUtil.RandomNeed();
    }
    
    
    private static List<Need> NeedList(Need need, Character character){
      List<Need> needList = new List<Need>();
      
      int val = character.ErgSpectrum.GetNeed(need);
      
      val -= character.Relations.Providing(need.CorrectedFor(character)).Count;
      
      for(int i = 0; i < val; i++){
        needList.Add(need);
      }
      
      return needList;
    }
    
    
    public static void GenerateAllyFor(Character character){
      
      Stand stand = GetSemiRandomStand(character, Config.ALLY_STAND_RANDOMIZER);
      
      List<Character> potentialAllies = GetPotentialAllies(character, stand);
      
      Character newFriend;
      
      if(potentialAllies.Count > 0){
        newFriend = potentialAllies[DataManager.RND(potentialAllies.Count -1)];
      }else{
        newFriend = CharacterGenerator
            .NewCitizen(stand, 
                        Stat.Allies);
      }
      
      //EngineState.CITY
        //.AddCharacter(newFriend);
      
      RelationsUtil
        .Acquaint(character, newFriend);
      RelationsUtil
        .Befriend(character, newFriend);
    }
    
    
    private static Stand GetSemiRandomStand(Character character, int randomizer){
      
      Stand stand = character.Stand;
      
      if(DataManager.RND(randomizer) == 0){
        stand = Stand.Any;
      }
      
      return stand;
    }
    
    
    public static List<Character> GetPotentialAllies(Character character){
      return GetPotentialAllies(character, null);
    }
    
    
    public static List<Character> GetPotentialAllies(Character character, Stand? stand){

      if(stand == null){
        //Todo: implement this method
        //stand = GetAllyStand(character);
      }

      Stand trueStand = (Stand)stand;
      
      List<Character> existingConns = character.Relations.GetEachCharacter(ConnectionType.Ally, ConnectionType.Contact);
      //todo: store these as a general purpose set
      
      List<Character> missingAllies = CharacterPicker.ThoseMissingAllies();
        
      List<Character> potentialAllies 
          = missingAllies
          .Where(x => x != character && x.Stand.Equivalent(trueStand) && !existingConns.Contains(x)).ToList();
        // may include vampires...
      
      return potentialAllies;
    }
    
    
  }
}