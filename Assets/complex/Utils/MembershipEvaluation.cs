using Vanitas.Components.Attributes;

namespace Vanitas.Components.Utils{
  public class MembershipEvaluation{
    
    public static float By(Clan clan, Character character){
      if (clan == Clan.Vrykolakas){
        return ByVrykolakas(character);
      } else if(clan == Clan.Shtriga){
        return ByShtriga(character);
      }
      
      return 0f;
    }
    
    public static float ByVrykolakas(Character character){
      float val = 0f;
      
      val += character.GetStat(Stat.Physical) * 1.7f;
      val += character.GetStat(Stat.Social) * 1.2f;
      val += character.GetStat(Stat.Mental) * 1.6f;

      //debug:
      val += character.GetStat(Stat.Physical) * 100f;

      //todo: add more criteria
      
      return val;
    }
    
    public static float ByShtriga(Character character){
      float val = 0f;
      
      val += character.GetStat(Stat.Physical) * 1.1f;
      val += character.GetStat(Stat.Social) * 1.6f;
      val += character.GetStat(Stat.Mental) * 1.3f;

      //debug:
      val += character.GetStat(Stat.Social) * 100f;

      //todo: add more criteria

      return val;
    }
    
  }
}