using System;

namespace Vanitas.Components.Utils{
  public enum ProgressModel{
    Natural,
    // 1,2,3,4,5,6,7...
    SequentialAddition,
    // 1,3,6,10,15,21,28...
    SelfMultiplication,
    // 1,4,9,16,25,36,49...
    Duplicative,
    // 1,2,4,8,16,32,64...
    Fibonacci,
    // 1,2,3,5,8,13,21...
    PowerOfTen
    // 1,10,100,1 000,10 000...
  }
  
  static class ProgressModelExtension{
    
    public static int Tier(this ProgressModel model, int ordinal){
      if(ordinal < 1){
        return 0;
      }
      
      if(model == ProgressModel.SequentialAddition){
        return CalcSeqAdd(ordinal);
      }else if(model == ProgressModel.SelfMultiplication){
        return CalcSelfMul(ordinal);
      }else if(model == ProgressModel.Duplicative){
        return CalcDupli(ordinal);
      }else if(model == ProgressModel.Fibonacci){
        return CalcFibo(ordinal);
      }else if(model == ProgressModel.PowerOfTen){
        return CalcPo10(ordinal);
      }
      
      return ordinal;
    }
    
    private static int CalcSeqAdd(int ordinal){
      int result = 0;
      for(int i = 1; i <= ordinal; i++){
        result += i;
      }
      return result;
    }
    
    private static int CalcSelfMul(int ordinal){
      return (ordinal * ordinal);
    }
    
    private static int CalcDupli(int ordinal){
      return (int)Math.Pow(2, ordinal -1);
    }
    
    private static int CalcFibo(int ordinal){
      
      int a = 0;
      int b = 1;
      
      for(int i = 0; i < ordinal; i++){
        int c = a + b;
        a = b;
        b = c;
      }
      
      return b;
    }
    
    private static int CalcPo10(int ordinal){
      if(ordinal == 1){
        return 1;
      }
      return (int)Math.Pow(10, ordinal -1);
    }
  }
}