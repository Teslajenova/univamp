using System.Collections.Generic;
using Vanitas.Components.Ambition;

namespace Vanitas.Components.Commodity{
  public enum Vendible{
    Art,
    Blood, //medical blood?
    Craft,
    Entertainment, //companionship?
    Housing,
    Investment,
    Secrets,
    Training
  }
  
  static class VendibleExtension{
    
    public static VendibleDataModel Data(this Vendible vendible){
      return DATA_MODELS[vendible];
    }
    
    public static Dictionary<Vendible, VendibleDataModel> DATA_MODELS = new Dictionary<Vendible, VendibleDataModel>(){
      {
        Vendible.Art,
        new VendibleDataModel(
          new HashSet<Need>(){
            Need.Relatedness,
            Need.Growth
          }
        )
      },
      {
        Vendible.Blood,
        new VendibleDataModel(
          new HashSet<Need>(){
            Need.VampiricExistence
          }
        )
      },
      {
        Vendible.Craft,
        new VendibleDataModel(
          new HashSet<Need>(){
            Need.Relatedness
          }
        )
      },
      {
        Vendible.Entertainment,
        new VendibleDataModel(
          new HashSet<Need>(){
            Need.Relatedness,
            Need.Growth
          }
        )
      },
      {
        Vendible.Housing,
        new VendibleDataModel(
          new HashSet<Need>(){
            Need.Existence,
            Need.Relatedness
          }
        )
      },
      {
        Vendible.Investment,
        new VendibleDataModel(
          new HashSet<Need>(){
            Need.Existence,
            Need.Relatedness
          }
        )
      },
      {
        Vendible.Secrets,
        new VendibleDataModel(
          new HashSet<Need>(){
            Need.Relatedness
          }
        )
      },
      {
        Vendible.Training,
        new VendibleDataModel(
          new HashSet<Need>(){
            Need.Existence,
            Need.Relatedness,
            Need.Growth
          }
        )
      }
      
    };
  }
}