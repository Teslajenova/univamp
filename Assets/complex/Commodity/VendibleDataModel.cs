using System.Collections.Generic;
using Vanitas.Components.Ambition;

namespace Vanitas.Components.Commodity{
  public class VendibleDataModel{
    
    private HashSet<Need> needsMet;
    
    public VendibleDataModel(HashSet<Need> needsMet){
      this.needsMet = needsMet;
    }
    
    public HashSet<Need> NeedsMet{
      get{
        return this.needsMet;
      }
    }
  }
}