namespace Vanitas.Components.Commodity.Util{
  public class PossessionCapacityCounter: BGUnitCounter{
    
    public int GetCount(Character character){
      return character.Possessions.Capacity;
    }
    
  }
}