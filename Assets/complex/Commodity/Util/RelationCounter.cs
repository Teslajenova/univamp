using Vanitas.Components.Linkage;

namespace Vanitas.Components.Commodity.Util{
  public class RelationCounter: BGUnitCounter{
    
    private ConnectionType connectionType;
    
    public RelationCounter(ConnectionType connectionType){
      this.connectionType = connectionType;
    }
    
    public int GetCount(Character character){
      return character.Relations.GetEach(connectionType).Count;
    }
    
  }
  
  /*
  public class RelationCounter<T>: BGUnitCounter where T: Link{
    
    public override int GetCount(Character character){
      return character.Relations.GetEach<T>().Count;
    }
    
  }
  */
}