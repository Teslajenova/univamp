namespace Vanitas.Components.Commodity.Util{
  public interface BGUnitCounter{
    
    int GetCount(Character character);
    
  }
}