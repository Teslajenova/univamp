using System.Collections.Generic;

namespace Vanitas.Components.Ambition{
  public class ErgSpectrum{
    
    private Dictionary<Need, int> needs = new Dictionary<Need, int>();
    
    public ErgSpectrum(){
      // maybe require attribute inputs?
      
      needs.Add(Need.Existence, DataManager.RND( Config.COMMON_ERG_SPECTRUM_MIN, Config.COMMON_ERG_SPECTRUM_MAX +1));
      
      needs.Add(Need.Relatedness, DataManager.RND( Config.COMMON_ERG_SPECTRUM_MIN, Config.COMMON_ERG_SPECTRUM_MAX +1));
      
      needs.Add(Need.Growth, DataManager.RND( Config.COMMON_ERG_SPECTRUM_MIN, Config.COMMON_ERG_SPECTRUM_MAX +1));
      
    }
    
    public int GetNeed(Need need){
      need = need.GetBaseForm();
      return needs[need];
    }
    
  }
}