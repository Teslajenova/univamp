namespace Vanitas.Components.Ambition{
  public enum Need{
    Existence,
    Relatedness,
    Growth,
    
    VampiricExistence
  }
  
  static class NeedExtension{
    
    public static Need GetBaseForm(this Need need){
      if(need == Need.VampiricExistence){
        return Need.Existence;
      }
      
      return need;
    }
    
    public static Need CorrectedFor(this Need need, Character character){
      if(character.IsVampire()){
        
        if(need == Need.Existence){
          return Need.VampiricExistence;
        }
      }//todo: maybe handle the reverse
      
      return need;
    }
    
    public static bool Contains(this Need thiz, Need other){
      if(thiz == other){
        return true;
      }
      
      if(thiz == Need.VampiricExistence && other == Need.Existence){
        return true;
      }
      
      return false;
    }
    
  }
  
}