

using System;
using System.Collections.Generic;

namespace Vanitas.Components.Attributes{
  public class TraitDataModel{
    
    private String name;
    private Sex sex;
    private Dictionary<Stat, int> statModifiers;
    //stand?
    
    public TraitDataModel(String name, Sex sex, Dictionary<Stat, int> statModifiers){
      this.name = name;
      this.sex = sex;
      this.statModifiers = statModifiers;
    }

    
    public Sex Sex {
      get { return this.sex; }
    }
    
    
    public int Stat(Stat stat){
      int val = 0;
      statModifiers.TryGetValue(stat, out val); 
      return val;
    }
    
  }
}