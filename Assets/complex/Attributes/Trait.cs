

using System;
using System.Collections.Generic;
using System.Linq;
using Vanitas.Components.Attributes;

namespace Vanitas.Components.Attributes{
  public enum Trait{
    Athletic,
    Clever,
    Dandy,
    Savvy,
    Shy,
    Thick,
    Weak
  }
  
  static class TraitExtension{
    
    
    public static Trait Random(){
        Array values = Enum.GetValues(typeof(Trait));
        return (Trait)values.GetValue(DataManager.RND(values.Length));
    }
 
    
    public static Trait Random(Sex sex){
      List<Trait> suitableTraits = GetAll(sex).ToList();
      
      return suitableTraits.ElementAt(DataManager.RND(suitableTraits.Count));
    }
 
    
    public static HashSet<Trait> GetAll(Sex sex){
      HashSet<Trait> traits = new HashSet<Trait>();
      
      foreach(Trait trait in Enum.GetValues(typeof(Trait))){
        if(trait.Data().Sex == Sex.None || trait.Data().Sex == sex){
          traits.Add(trait);
        }
      }
      
      return traits;
    }
 
    
    public static TraitDataModel Data(this Trait trait){
      return DATA_MODELS[trait];
    }

    
    public static Dictionary<Trait, TraitDataModel> DATA_MODELS = new Dictionary<Trait, TraitDataModel>(){
      {
        Trait.Athletic,
        new TraitDataModel(
          "Athletic",
          Sex.None,
          new Dictionary<Stat, int>(){
            {Stat.Physical, 1}
          }
        )
      },
      {
        Trait.Clever,
        new TraitDataModel(
          "Clever",
          Sex.None,
          new Dictionary<Stat, int>(){
            {Stat.Mental, 1}
          }
        )
      },
      {
        Trait.Dandy,
        new TraitDataModel(
          "Dandy",
          Sex.Male,
          new Dictionary<Stat, int>(){
            {Stat.Physical, -1},
            {Stat.Social, 1}
          }
        )
      },
      {
        Trait.Savvy,
        new TraitDataModel(
          "Savvy",
          Sex.None,
          new Dictionary<Stat, int>(){
            {Stat.Social, 1}
          }
        )
      },
      {
        Trait.Shy,
        new TraitDataModel(
          "Shy",
          Sex.None,
          new Dictionary<Stat, int>(){
            {Stat.Social, -1}
          }
        )
      },
      {
        Trait.Thick,
        new TraitDataModel(
          "Thick",
          Sex.None,
          new Dictionary<Stat, int>(){
            {Stat.Mental, -1}
          }
        )
      },
      {
        Trait.Weak,
        new TraitDataModel(
          "Weak",
          Sex.None,
          new Dictionary<Stat, int>(){
            {Stat.Physical, -1}
          }
        )
      }

    };
    
  }
}