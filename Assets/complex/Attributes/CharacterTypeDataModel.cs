

using System;
using System.Collections.Generic;
using Vanitas.Components.Ambition;
using Vanitas.Components.Commodity;

namespace Vanitas.Components.Attributes{
  public class CharacterTypeDataModel{
    
    private String name;
    private Sex sex;
    private Stand stand;
    
    private HashSet<StatRange> statRanges;
    
    private HashSet<Vendible> vendibles;
    
    // graphics
    
    public CharacterTypeDataModel(String name, Sex sex, Stand stand, HashSet<StatRange> statRanges, HashSet<Vendible> vendibles){
      this.name = name;
      this.sex = sex;
      this.stand = stand;
      this.statRanges = statRanges;
      this.vendibles = vendibles;
    }
    
    /*
    public CharacterTypeDataModel(String name, Sex sex, Stand stand, Dictionary<Stat, int> baselineStats){
      this.name = name;
      this.sex = sex;
      this.stand = stand;
      this.baselineStats = baselineStats;
    }
    */
    
    /*
    public int Stat(Stat stat){
      int val = 0;
      baselineStats.TryGetValue(stat, out val);
      return val;
    }
    */
    
    public HashSet<StatRange> StatRanges(){
      return this.statRanges;
    }
    
    public String Name{
      get{
        return this.name;
      }
    }
    
    
    public HashSet<Vendible> Vendibles{
      get{
        return this.vendibles;
      }
    }
    
    
    public HashSet<Need> MatchedNeeds(){
      HashSet<Need> needs = new HashSet<Need>();
      
      foreach(Vendible vendible in Vendibles){
        foreach(Need need in vendible.Data().NeedsMet){
          needs.Add(need);
        }
      }
      
      return needs;
    }
    
    
    public Sex Sex{
      get{
        return this.sex;
      }
    }
    
    
    public Stand Stand{
      get{
        return this.stand;
      }
    }
  }
}