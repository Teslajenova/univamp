

namespace Vanitas.Components.Attributes{
  public enum Sex{
    None,
    Female,
    Male
  }
}