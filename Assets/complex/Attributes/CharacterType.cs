using System;
using System.Collections.Generic;
using System.Linq;
using Vanitas.Components.Attributes;
using Vanitas.Components.Commodity;

namespace Vanitas.Components.Attributes{
  
  public enum CharacterType{
    Casanova,
    Cleaner,
    Clerk,
    Consultant,
    Dilettante,
    Entrepreneur,
    FactoryWorker,
    //FemmeFatale,
    Manager,
    Painter,
    //PrivateInvestigator,
    Secretary,
    Tycoon,
    //Vagrant,
    Waitress
  }
  
  static class CharacterTypeExtension{
    
    public static CharacterType Random(){
      Array values = Enum.GetValues(typeof(CharacterType));
      
      return (CharacterType)values.GetValue(DataManager.RND(values.Length));
    }
    
    public static CharacterType Random(Stand stand){
      List<CharacterType> equivalentTypes = GetAll(stand).ToList();
      
      return equivalentTypes.ElementAt(DataManager.RND(equivalentTypes.Count));
    }
    
    public static HashSet<CharacterType> GetAll(Stand stand){
      HashSet<CharacterType> types = new HashSet<CharacterType>();
      
      foreach(CharacterType type in Enum.GetValues(typeof(CharacterType))){
        if(type.Data().Stand.Equivalent(stand)){
          types.Add(type);
        }
      }
      
      return types;
    }
    
    public static CharacterTypeDataModel Data(this CharacterType type){
      return DATA_MODELS[type];
    }
    
    public static Dictionary<CharacterType, CharacterTypeDataModel> DATA_MODELS = new Dictionary<CharacterType, CharacterTypeDataModel>(){
      {
        CharacterType.Casanova,
        new CharacterTypeDataModel(
          "Casanova",
          Sex.Male,
          Stand.Any,
          new HashSet<StatRange>(){
            new StatRange(Stat.Physical,2,2),
            new StatRange(Stat.Social,4,4),
            new StatRange(Stat.Mental,2,2),
            
            new StatRange(Stat.Allies,0,2),
            new StatRange(Stat.Contacts,0,2),
            new StatRange(Stat.Fame,0,2),
            new StatRange(Stat.Resources,1,3)
          },
          new HashSet<Vendible>()
        )
      },
      {
        CharacterType.Cleaner,
        new CharacterTypeDataModel(
          "Cleaner",
          Sex.Male,
          Stand.Lower,
          new HashSet<StatRange>(){
            new StatRange(Stat.Physical,4,4),
            new StatRange(Stat.Social,1,1),
            new StatRange(Stat.Mental,3,3),
            
            new StatRange(Stat.Allies,1,2),
            new StatRange(Stat.Contacts,0,2),
            new StatRange(Stat.Resources,1,1)
          },
          new HashSet<Vendible>(){
            Vendible.Secrets
          }
        )
      },
      {
        CharacterType.Clerk,
        new CharacterTypeDataModel(
          "Clerk",
          Sex.Male,
          Stand.Middle,
          new HashSet<StatRange>(){
            new StatRange(Stat.Physical,2,2),
            new StatRange(Stat.Social,3,3),
            new StatRange(Stat.Mental,3,3),
            
            new StatRange(Stat.Allies,0,2),
            new StatRange(Stat.Contacts,0,1),
            new StatRange(Stat.Resources,2,3)
          },
          new HashSet<Vendible>(){
            Vendible.Secrets
          }
        )
      },
      {
        CharacterType.Consultant,
        new CharacterTypeDataModel(
          "Consultant",
          Sex.Female,
          Stand.Middle,
          new HashSet<StatRange>(){
            new StatRange(Stat.Physical,1,1),
            new StatRange(Stat.Social,4,4),
            new StatRange(Stat.Mental,3,3),
            
            new StatRange(Stat.Allies,0,2),
            new StatRange(Stat.Contacts,2,3),
            new StatRange(Stat.Fame,0,2),
            new StatRange(Stat.Influence,0,1),
            new StatRange(Stat.Resources,1,3)
          },
          new HashSet<Vendible>(){
            Vendible.Training
          }
        )
      },
      {
        CharacterType.Dilettante,
        new CharacterTypeDataModel(
          "Dilettante",
          Sex.Female,
          Stand.Upper,
          new HashSet<StatRange>(){
            new StatRange(Stat.Physical,2,2),
            new StatRange(Stat.Social,3,3),
            new StatRange(Stat.Mental,3,3),
            
            new StatRange(Stat.Allies,0,2),
            new StatRange(Stat.Contacts,0,2),
            new StatRange(Stat.Fame,0,1),
            new StatRange(Stat.Influence,0,1),
            new StatRange(Stat.Resources,3,5)
          },
          new HashSet<Vendible>()
        )
      },
      {
        CharacterType.Entrepreneur,
        new CharacterTypeDataModel(
          "Entrepreneur",
          Sex.Male,
          Stand.MiddleToUpper,
          new HashSet<StatRange>(){
            new StatRange(Stat.Physical,3,3),
            new StatRange(Stat.Social,2,2),
            new StatRange(Stat.Mental,3,3),
            
            new StatRange(Stat.Allies,0,2),
            new StatRange(Stat.Contacts,1,3),
            new StatRange(Stat.Fame,0,1),
            new StatRange(Stat.Influence,0,2),
            new StatRange(Stat.Resources,3,5)
          },
          new HashSet<Vendible>(){
            Vendible.Investment
          }
        )
      },
      {
        CharacterType.FactoryWorker,
        new CharacterTypeDataModel(
          "Factory Worker",
          Sex.Male,
          Stand.LowerToMiddle,
          new HashSet<StatRange>(){
            new StatRange(Stat.Physical,4,4),
            new StatRange(Stat.Social,2,2),
            new StatRange(Stat.Mental,2,2),
            
            new StatRange(Stat.Allies,1,3),
            new StatRange(Stat.Resources,1,2)
          },
          new HashSet<Vendible>(){
            Vendible.Craft
          }
        )
      },
      /*
      {
        CharacterType.FemmeFatale, 
        new CharacterTypeDataModel(
          "Femme Fatale",
          Sex.Female,
          Stand.Any,
          new Dictionary<Stat, int>(){
            {Stat.Physical, 2},
            {Stat.Social, 4},
            {Stat.Mental, 2}
          }
        )
      },
      */
      {
        CharacterType.Manager,
        new CharacterTypeDataModel(
          "Manager",
          Sex.Female,
          Stand.MiddleToUpper,
          new HashSet<StatRange>(){
            new StatRange(Stat.Physical,2,2),
            new StatRange(Stat.Social,2,2),
            new StatRange(Stat.Mental,4,4),
            
            new StatRange(Stat.Allies,0,1),
            new StatRange(Stat.Contacts,1,2),
            new StatRange(Stat.Influence,1,1),
            new StatRange(Stat.Resources,2,3)
          },
          new HashSet<Vendible>(){
            Vendible.Secrets
          }
        )
      },
      {
        CharacterType.Painter,
        new CharacterTypeDataModel(
          "Painter",
          Sex.Female,
          Stand.Any,
          new HashSet<StatRange>(){
            new StatRange(Stat.Physical,1,1),
            new StatRange(Stat.Social,2,2),
            new StatRange(Stat.Mental,5,5),
            
            new StatRange(Stat.Allies,0,1),
            new StatRange(Stat.Contacts,1,2),
            new StatRange(Stat.Fame,0,2),
            new StatRange(Stat.Resources,1,3)
          },
          new HashSet<Vendible>(){
            Vendible.Art
          }
        )
      },
      /*
      {
        CharacterType.PrivateInvestigator,
        new CharacterTypeDataModel(
          "Private Investigator",
          Sex.Male,
          Stand.LowerToMiddle,
          new Dictionary<Stat, int>(){
            {Stat.Physical, 4},
            {Stat.Social, 1},
            {Stat.Mental, 3}
          }
        )
      },
      */
      {
        CharacterType.Secretary,
        new CharacterTypeDataModel(
          "Secretary",
          Sex.Female,
          Stand.LowerToMiddle,
          new HashSet<StatRange>(){
            new StatRange(Stat.Physical,2,2),
            new StatRange(Stat.Social,4,4),
            new StatRange(Stat.Mental,2,2),
            
            new StatRange(Stat.Allies,1,3),
            new StatRange(Stat.Contacts,0,2),
            new StatRange(Stat.Influence,0,1),
            new StatRange(Stat.Resources,1,2)
          },
          new HashSet<Vendible>(){
            Vendible.Secrets
          }
        )
      },
      {
        CharacterType.Tycoon,
        new CharacterTypeDataModel(
          "Tycoon",
          Sex.Male,
          Stand.Upper,
          new HashSet<StatRange>(){
            new StatRange(Stat.Physical,1,1),
            new StatRange(Stat.Social,3,3),
            new StatRange(Stat.Mental,4,4),
            
            new StatRange(Stat.Allies,0,2),
            new StatRange(Stat.Contacts,1,2),
            new StatRange(Stat.Fame,0,2),
            new StatRange(Stat.Influence,1,2),
            new StatRange(Stat.Resources,4,5)
          },
          new HashSet<Vendible>(){
            Vendible.Investment
          }
        )
      },
      /*
      {
        CharacterType.Vagrant,
        new CharacterTypeDataModel(
          "Vagrant",
          Sex.Male,
          Stand.Lower,
          new Dictionary<Stat, int>(){
            {Stat.Physical, 4},
            {Stat.Social, 1},
            {Stat.Mental, 2}
          }
        )
      },
      */
      {
        CharacterType.Waitress,
        new CharacterTypeDataModel(
          "Waitress",
          Sex.Female,
          Stand.Lower,
          new HashSet<StatRange>(){
            new StatRange(Stat.Physical,3,3),
            new StatRange(Stat.Social,3,3),
            new StatRange(Stat.Mental,2,2),
            
            new StatRange(Stat.Allies,1,3),
            new StatRange(Stat.Resources,1,1)
          },
          new HashSet<Vendible>()
        )
      }
    };
    
  }
  
}