

using System.Collections.Generic;
using Vanitas.Components.Commodity.Util;
using Vanitas.Components.Linkage;
using Vanitas.Components.Utils;

namespace Vanitas.Components.Attributes{
  public enum Stat{
    // main stats / attributes
    Physical,
    Social,
    Mental,
    
    // backgrounds
    Allies,
    Contacts,
    Fame,
    Influence,
    Resources
  }
  
  static class StatExtension{
    
    public static bool IsPrimed(this Stat stat, Character character){
      return BackgroundChecker
        .BackgroundIsPrimed(stat, character);
    }
    
    /*
    public static int CurrentUnitAmount(this Stat stat, Character character){
      
      return BackgroundChecker
        .GetCurrentUnitAmount(stat, character);
    }
    */
    
    public static StatDataModel Data(this Stat stat){
      return DATA_MODELS[stat];
    }

    public static Dictionary<Stat, StatDataModel> DATA_MODELS = new Dictionary<Stat, StatDataModel>(){
      //Attributes
      {
        Stat.Physical,
        new StatDataModel(
          null, null, null
        )
      },
      {
        Stat.Social,
        new StatDataModel(
          null, null, null
        )
      },
      {
        Stat.Mental,
        new StatDataModel(
          null, null, null
        )
      },
      
      //Backgrounds
      {
        Stat.Allies,
        new StatDataModel(
          new RelationCounter(ConnectionType.Ally),
          ProgressModel.Natural, 1
        )
      },
      {
        Stat.Contacts,
        new StatDataModel(
          new RelationCounter(ConnectionType.Contact),
          ProgressModel.Natural, 1
        )
      },
      {
        Stat.Fame,
        new StatDataModel(
          null, // todo fix this
          ProgressModel.SelfMultiplication, 5
        )
      },
      {
        Stat.Influence,
        new StatDataModel(
          null, // todo fix this
          ProgressModel.Natural, 1
        )
      },
      {
        Stat.Resources,
        new StatDataModel(
          new PossessionCapacityCounter(),
          ProgressModel.Duplicative, 10
        )
      }

    };
  }
}