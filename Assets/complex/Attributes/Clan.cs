using System;
using System.Collections.Generic;

namespace Vanitas.Components.Attributes{
  public enum Clan{
    Vrykolakas,
    Shtriga // Glaistig?
  }
  
  public static class ClanExtension{
    
    public static Clan Random(){
      Array values = Enum.GetValues(typeof(Clan));
      
      return (Clan)values.GetValue(DataManager.RND(values.Length));
    }
    
    
    public static ClanDataModel Data(this Clan clan){
      return DATA_MODELS[clan];
    }

    
    public static Dictionary<Clan, ClanDataModel> DATA_MODELS = new Dictionary<Clan, ClanDataModel>(){
      {
        Clan.Vrykolakas,
        new ClanDataModel(
          "Vrykolakas",
          new HashSet<Discipline>(){
            Discipline.Clip,
            Discipline.Bent,
            Discipline.Aura
          }
        )
      },
      {
        Clan.Shtriga,
        new ClanDataModel(
          "Shtriga",
          new HashSet<Discipline>(){
            Discipline.Insight,
            Discipline.Clip,
            Discipline.Aura
          }
        )
      }
    };
    
  }
}