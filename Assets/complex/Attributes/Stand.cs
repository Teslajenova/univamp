namespace Vanitas.Components.Attributes { 
  public enum Stand{
    Any,
    Lower,
    LowerToMiddle,
    Middle,
    MiddleToUpper,
    Upper
  }
  
  public static class StandExtension{
    
    public static bool Equivalent(this Stand s1, Stand s2){
      if (s1 == Stand.Any || s2 == Stand.Any || s1 == s2){
        return true;
      } else if (s1 == Stand.Lower){
        if (s2 == Stand.LowerToMiddle){
          return true;
        }
      } else if (s1 == Stand.LowerToMiddle){
        if (s2 == Stand.Lower || s2 == Stand.Middle){
          return true;
        }
      } else if (s1 == Stand.Middle){
        if (s2 == Stand.LowerToMiddle || s2 == Stand.MiddleToUpper){
          return true;
        }
      } else if (s1 == Stand.MiddleToUpper){
        if (s2 == Stand.Middle || s2 == Stand.Upper){
          return true;
        }
      } else if (s1 == Stand.Upper){
        if (s2 == Stand.MiddleToUpper){
          return true;
        }
      }
    
      return false;
    }
  }
}