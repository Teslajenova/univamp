using System;
using System.Collections.Generic;

namespace Vanitas.Components.Attributes{
  public class ClanDataModel{
    
    private HashSet<Discipline> disciplines;
    
    private String name;
    
    public ClanDataModel(String name, HashSet<Discipline> disciplines){
      this.name = name;
      this.disciplines = disciplines;
    }
    
  }
}