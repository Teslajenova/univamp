namespace Vanitas.Components.Attributes{
  public enum Discipline{
    Insight,
    Clip,
    Bent,
    Aura
  }
}