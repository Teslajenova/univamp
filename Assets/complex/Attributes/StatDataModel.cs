using Vanitas.Components.Commodity.Util;
using Vanitas.Components.Utils;

namespace Vanitas.Components.Attributes{
  public class StatDataModel{
    
    private BGUnitCounter unitCounter = null;
    
    //private bool isBackground;
    private ProgressModel? progressModel = null;
    private int? unitCountMultiplier = null;
    
    
    public StatDataModel(BGUnitCounter unitCounter, ProgressModel? progressModel, int? unitCountMultiplier){
      this.unitCounter = unitCounter;
      //this.isBackground = isBackground;
      this.progressModel = progressModel;
      this.unitCountMultiplier = unitCountMultiplier;
    }
    
    
    public int? CurrentCount(Character character){
      if(unitCounter == null){
        return null;
      }
      
      return unitCounter.GetCount(character);
    }
    
    
    public int? RandomValidCountForValue(int val){
      int? lowerBound = UnitCountForValue(val);
      
      if(lowerBound == null){
        return null;
      }
      
      int? upperBound = UnitCountForValue(val +1);
      
      return DataManager.RND((int)lowerBound, (int)upperBound);
      
    }
    
    
    public int? UnitCountForValue(int val){
      if(progressModel == null){
        return null;
      }

      ProgressModel pm = (ProgressModel)progressModel;
      return unitCountMultiplier * pm.Tier(val);
    }
    
    
    /*
    public bool IsBackground{
      get{
        return this.isBackground;
      }
    }
    */
    
  }
}