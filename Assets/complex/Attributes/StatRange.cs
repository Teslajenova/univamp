

namespace Vanitas.Components.Attributes{
  public class StatRange{
    
    private Stat stat;
    private int min;
    private int max;
    //private int val;
    
    
    public StatRange(Stat stat, int min, int max){
      this.stat = stat;
      this.min = min;
      this.max = max;
      
      //this.val = GameCore.RANDOM.Next(min, max+1);
    }
    
    
    public int GetValue(){
      return DataManager.RND(min, max+1);
    }
    
    
    public Stat Stat{
      get{
        return this.stat;
      }
    }
    
  }
}