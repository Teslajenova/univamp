namespace Vanitas.Components.Linkage{
  public abstract class BooleanAspect :ConnectionAspect{
    
    protected bool state;
    
    public BooleanAspect(bool state){
      SetState(state);
    }
    
    public virtual bool GetState(){
      return this.state;
    }
    
    public virtual void SetState(bool state){
      this.state = state;
    }
    
    public override bool Consume(ConnectionAspect other){
      if(!SameTypeAs(other)){
        return false;
      }
      
      bool otherState = ((BooleanAspect)other).GetState();
      
      if(otherState == true){
        state = true;
      }
      
      return true;
    }
    
  }
}