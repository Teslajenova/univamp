using System.Collections.Generic;
using Vanitas.Components.Linkage.Aspects;

namespace Vanitas.Components.Linkage{
  public enum ConnectionType{ // is this class required?
    Sire,
    Childe,
    
    Regnant, // blood bond
    Thrall,
    
    Acquaintance,
    
    Ally,
    
    Contact,
    Emptor
  }
  
  static class ConnectionTypeExtension{
    
    public static HashSet<ConnectionAspect> AspectSet(this ConnectionType connectionType){
      HashSet<ConnectionAspect> aspectSet = new HashSet<ConnectionAspect>();
      
      if(connectionType == ConnectionType.Regnant){
        aspectSet.Add(new Rating(1,0,3));
      }
      
      return aspectSet;
    }
    
  }
  
}