namespace Vanitas.Components.Linkage{
  public abstract class RatedLink: Link{
    
    protected int rate;
    protected int minRate = 0;
    protected int maxRate = 0;
    
    public RatedLink(Character target, int minRate, int maxRate): base(target){
      this.minRate = minRate;
      this.maxRate = maxRate;
    }
    
    public void SetRate(int newRate){
      rate = newRate;
      
      if(rate < minRate){
        rate = minRate;
      }else if(rate > maxRate){
        rate = maxRate;
      }
    }
    
    public void ModifyRateBy(int amount){
      SetRate(rate + amount);
    }
    
    public override bool Consume(Link other){
      if(!base.Consume(other)){
        return false;
      }

      ModifyRateBy(((RatedLink)other).Rate);
      return true;
    }
    
    public int Rate{
      get{
        return this.rate;
      }
    }
    
  }
}