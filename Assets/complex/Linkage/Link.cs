namespace Vanitas.Components.Linkage{
  public abstract class Link{
    
    protected Character target;
    
    //Add timestamp here
    
    public Link(Character target){
      this.target = target;
    }
    
    public Character Target{
      get{
        return this.target;
      }
    }
    
    public virtual bool Consume(Link other){
      if(!Duplicates(other)){
        return false;
      }
      
      // possible logic goes here
      return true;
    }
    
    public bool Duplicates(Link other){
      
      if(!this.GetType().Equals(other.GetType())){
        return false;
      }
      
      if(this.Target != other.Target){
        return false;
      }
      
      return true;
    }
    
  }
  
  /*
  public enum Link{
    Childe,
    Sire,
    
    BloodBound_1,
    BloodBound_2,
    BloodBound_3,
    Thrall,
      
    Ally,
    MajorContact,
    MinorContact,
    Idol,
    Fan
  }
  */
}