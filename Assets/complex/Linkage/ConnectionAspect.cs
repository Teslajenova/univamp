namespace Vanitas.Components.Linkage{
  public abstract class ConnectionAspect{
    
    public abstract bool Consume(ConnectionAspect other);
    
    public virtual bool SameTypeAs(ConnectionAspect other){
      return this.GetType().Equals(other.GetType());
    }
    
  }
}