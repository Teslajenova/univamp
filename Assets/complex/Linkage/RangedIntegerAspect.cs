namespace Vanitas.Components.Linkage{
  public abstract class RangedIntegerAspect :IntegerAspect{
    
    protected int min;
    protected int max;
    
    public RangedIntegerAspect(int val, int min, int max) :base(val){
      this.min = min;
      this.max = max;
      SetVal(val);
    }
    
    public override void SetVal(int val){
      base.SetVal(val);
      ConstrainVal();
    }
    
    public override void ModifyVal(int mod){
      base.ModifyVal(mod);
      ConstrainVal();
    }
    
    protected virtual void ConstrainVal(){
      if(val < min){
        val = min;
      }else if(val > max){
        val = max;
      }
    }
    
  }
}