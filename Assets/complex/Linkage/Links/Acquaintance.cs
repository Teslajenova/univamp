namespace Vanitas.Components.Linkage.Links{
  public class Acquaintance: Connection{
    
    
    public Acquaintance(Character target) : base(ConnectionType.Acquaintance, target) {
      
    }
  }
}