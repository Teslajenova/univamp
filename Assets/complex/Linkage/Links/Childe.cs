namespace Vanitas.Components.Linkage.Links{
  public class Childe: Connection{
    
    public Childe(Character target) : base(ConnectionType.Childe, target) {
      
    }
    
  }
}