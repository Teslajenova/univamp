namespace Vanitas.Components.Linkage.Links{
  public class Contact: Connection{
    
    public Contact(Character target): base(ConnectionType.Contact, target){
      
    }
    
  }
}