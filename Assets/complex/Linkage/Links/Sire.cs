namespace Vanitas.Components.Linkage.Links{
  public class Sire: Connection{

    public Sire(Character target) : base(ConnectionType.Sire, target) {

    }

  }
}