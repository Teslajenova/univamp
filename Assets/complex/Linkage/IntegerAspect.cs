namespace Vanitas.Components.Linkage{
  public abstract class IntegerAspect :ConnectionAspect{
    
    protected int val;
    
    public IntegerAspect(int val){
      SetVal(val);
    }
    
    public virtual int GetVal(){
      return this.val;
    }
    
    public virtual void SetVal(int val){
      this.val = val;
    }
    
    public virtual void ModifyVal(int mod){
      this.val += mod;
    }
    
    public override bool Consume(ConnectionAspect other){
      if(!SameTypeAs(other)){
        return false;
      }
      
      int otherVal = ((IntegerAspect)other).GetVal();
      
      ModifyVal(otherVal);

      return true;
    }
    
  }
}