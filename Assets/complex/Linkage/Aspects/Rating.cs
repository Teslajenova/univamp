using System;

namespace Vanitas.Components.Linkage.Aspects{
  
  public class Rating :RangedIntegerAspect{
    
    public Rating(int val, int min, int max) :base(val, min, max){
      
    }

  }
  
}