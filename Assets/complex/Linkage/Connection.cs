using System;
using System.Collections.Generic;

namespace Vanitas.Components.Linkage{
  public class Connection{
    
    protected ConnectionType connectionType; // is this really neeeded?
    
    protected Character target;
    
    protected HashSet<ConnectionAspect> aspects;
    
    public Connection(ConnectionType connectionType, Character target){
      
      this.connectionType = connectionType;
      
      this.target = target;
      
      this.aspects = connectionType.AspectSet();
    }
    
    public virtual bool Consume(Connection other){
      if(!Duplicates(other)){
        return false;
      }
      
      foreach(ConnectionAspect aspect in aspects){
        ConnectionAspect foreingAspect = other.GetAspectByType(aspect.GetType());
        
        aspect.Consume(foreingAspect);
      }
      
      return true;
    }
    
    public bool Duplicates(Connection other){
      
      if(this.Type != other.Type){
        return false;
      }
      
      if(this.Target != other.Target){
        return false;
      }
      
      return true;
    }
    
    public ConnectionAspect GetAspectByType(Type concreteType){
      foreach(ConnectionAspect aspect in aspects){
        if(aspect.GetType().Equals(concreteType)){
          return aspect;
        }
      }
      
      return null;
    }
    
    public ConnectionType Type{
      get{
        return this.connectionType;
      }
    }
    
    public Character Target{
      get{
        return this.target;
      }
    }
    
    public HashSet<ConnectionAspect> Aspects{
      get{
        return this.aspects;
      }
    }
    
  }
}