using System;
using System.Collections.Generic;
using System.Linq;
using Vanitas.Components.Ambition;
using Vanitas.Components.Linkage.Links;

namespace Vanitas.Components.Linkage{
  public class Relations{
    
    private Character host;
    
    private HashSet<Connection> connections = new HashSet<Connection>();
    
    //private HashSet<Link> links = new HashSet<Link>();
    
    //private Dictionary<Character, HashSet<Link>> personRelations = new Dictionary<Character, HashSet<Link>>();
    
    //private Dictionary<Link, HashSet<Character>> relationPersons = new Dictionary<Link, HashSet<Character>>();
    
    public Relations(Character host){
      this.host = host;
    }
    
    public bool Add(Connection newConnection){
      if(Disallowed(newConnection)){
        return false;
      }
      
      // consume duplicates
      List<Connection> duplicates = connections.Where(x => x.Duplicates(newConnection)).ToList();
      
      if(duplicates.Count > 0){
        // duplicates.Count should never be more than 1. todo: make sure it won't be
        duplicates[0].Consume(newConnection);
        return true; // return false?
      }
      
      // add
      connections.Add(newConnection);
      Data.INSTANCE.City.AddCharacter(newConnection.Target);
      return true;
    }
    
    
    private bool Disallowed(Connection connection){
      if(connection.Target == host){
        return true;
      }
      
      if(connection.Type == ConnectionType.Sire 
        && GetEach(ConnectionType.Sire) != null 
        && GetEach(ConnectionType.Sire).Count > 0){
        return true;
      }
      
      return false;
    }
    
    
    public List<Connection> GetAllFor(Character character){
      return connections.Where(x => x.Target == character).ToList();
    }
    
    
    public List<Connection> GetEach(params ConnectionType[] connectionTypes){
      List<Connection> connsOfType = new List<Connection>();
      
      for(int i = 0; i < connectionTypes.Length; i++){
        connsOfType.AddRange( connections.Where(x => x.Type == connectionTypes[i]));
      }
      
      return connsOfType;
    }
    
    
    public List<Character> GetEachCharacter(params ConnectionType[] connectionTypes){
      List<Connection> connsOfType = GetEach(connectionTypes);
      List<Character> characters = new List<Character>();
      
      foreach(Connection connection in connsOfType){
        characters.Add(connection.Target);
      }
      
      return characters;
    }
    
    
    public List<Connection> Providing(Need need){
      need = need.CorrectedFor(host);
      
      return connections.Where(x => x.Target.Provides(need)).ToList();
    }
    
    
    public List<Character> CharactersProviding(Need need){
      List<Connection> conns = Providing(need);
      
      List<Character> characters = new List<Character>();
      
      foreach(Connection connection in conns){
        characters.Add(connection.Target);
      }
      
      return characters;
    }
    
    
    public List<Connection> GetAll(){
      return connections.ToList();
    }
    
    
    public List<Character> GetAllCharacters(){
      List<Character> characters = new List<Character>();
      
      foreach(Connection connection in connections){
        characters
          .Add(connection.Target);
      }
      
      return characters;
    }
    
    
    //Utility methods
    public Character GetSire(){
      List<Connection> sireList = GetEach(ConnectionType.Sire);
      
      if(sireList.Count == 0){
        return null;
      }
      
      return sireList[0].Target;
    }
    
    
    public List<Character> GetAncestry(){
      List<Character> ancestry = new List<Character>();
      
      Character sire = GetSire();
      
      if(sire != null){
        ancestry.Add(sire);
        
        while(ancestry[ancestry.Count -1].Relations.GetSire() != null){
          Character previous = ancestry[ancestry.Count -1].Relations.GetSire();
          
          ancestry.Add(previous);
        }
      }
      
      return ancestry;
    }
    
    /*
    public void Add(Link link){
      // check for disallowed multiples(sire)
      if(link is Sire &&
         GetEach<Sire>() != null &&
         GetEach<Sire>().Count > 0){
        return;
      }
      
      List<Link> duplicates = links.Where(x => x.Duplicates(link)).ToList();
      
      if(duplicates.Count > 0){
        duplicates[0].Consume(link);
        return;
      }
      
      links.Add(link);
    }
    */
    
    /*
    public void Add(Link link, Character character){
      // this check might change...
      if(link == Link.Sire && GetEach(link) != null && GetEach(link).Count > 0){
        return;
      }

      //dictionary 1
      if(personRelations.ContainsKey(character)) {
        personRelations[character].Add(link);
      }else{
        HashSet<Link> links = new HashSet<Link>();
        links.Add(link);
        personRelations.Add(character, links);
      }
        
      //dictionary 2
      if(relationPersons.ContainsKey(link)){
        relationPersons[link].Add(character);
      }else{
        HashSet<Character> characters = new HashSet<Character>();
        characters.Add(character);
        relationPersons.Add(link, characters);
      }
    }
    */
    
    /*
    public List<Link> GetAllFor(Character character){
      return links.Where(x => x.Target == character).ToList();
    }
    
    public List<Link> GetEach<T>(){
      return links.Where(x => x is T).ToList();
    }

    public List<Character> GetEachCharacter<T>() {
      List<Link> links = GetEach<T>();
      List<Character> characters = new List<Character>();

      foreach(Link link in links) {
        characters.Add(link.Target);
      }

      return characters;
    }
    */
    
    /*
    public Character GetSire(){
      List<Link> sireList = GetEach<Sire>();
      
      if(sireList.Count == 0){
        return null;
      }
      
      return sireList[0].Target;
    }
    */
    
    /*
    public HashSet<Character> GetEach(Link link){
      if(!relationPersons.ContainsKey(link)) {
                return new HashSet<Character>();
      }
      return relationPersons[link];
    }
    */
  }
}