using System;
using System.Collections.Generic;
using System.Linq;
using Vanitas.Components.Attributes;
using Vanitas.Components.Utils;

namespace Vanitas.Components{
  public class City{

    

    private int numberOfPlayers;
    
    private DateTime date;
    
    private Character[] protagonists = new Character[Config.MAX_NUMBER_OF_PLAYERS +1]; 
    // one extra so that indexing can begin at 1
    
    private HashSet<Character> characters = new HashSet<Character>();
   
    
    public City(int numberOfPlayers){
      this.numberOfPlayers = 
        numberOfPlayers;

      this.date = DateTime.Today;

      //InitCharacters();
    }

    
    public void InitCharacters() {
      AddStratifiedCitizens(Config.INITIAL_CITIZENS);
      CreateVampires(Config.INITIAL_NPC_VAMPIRES);
    }
    
    
    private void AddStratifiedCitizens(int amount){
 
      int upperClassCount = (int)Math.Round(amount * Config.UPPER_CLASS_RATIO);
      int lowerClassCount = (int)Math.Round(amount * Config.LOWER_CLASS_RATIO);
      int middleClassCount = amount - upperClassCount - lowerClassCount;
      
      AddCitizens(Stand.Upper, upperClassCount);
      AddCitizens(Stand.Middle, middleClassCount);
      AddCitizens(Stand.Lower, lowerClassCount);
      
      }
      
    
    private void AddCitizens(Stand stand, int count){
      for(int i = 0; i < count; i++){
        characters.Add(CharacterGenerator.NewCitizen(stand));
      }
    }
    
    
    public void AddCharacter(Character character){
      this.characters.Add(character);
    }
    
    
    private void CreateVampires(int count){
      for(int i = 0; i < count; i++){
        Clan clan = ClanExtension.Random();
        
        CharacterGenerator.NewVampire(clan, this);
      }
      
    }
    
    
    public int NumberOfPlayers{
      get{return this.numberOfPlayers;}
    }

    
    public List<Character> GetCharacters(){
      return this.characters.ToList();
    }
    
    
    public HashSet<Character> Characters {
        get { 
          return new HashSet<Character>(this.characters);
        }
    }
    
    
    public HashSet<Character> GetVampires(){
      return (HashSet<Character>)characters.Where(x => x.IsVampire());
    }
    
    
    public HashSet<Character> GetMortals(){
      HashSet<Character> mortals = new HashSet<Character>();

      foreach(Character character in characters) {
        if(!character.IsVampire()) {
          mortals.Add(character);
        }
      }

      return mortals;

      //return (HashSet<Character>)characters.Where(x => {return x.IsVampire() == false;});
      // Todo: try this
    }
    
    
    public HashSet<Character> GetMortals(Stand stand){
      HashSet<Character> allMortals = GetMortals();
      
      if(stand == Stand.Any){
        return allMortals;
      }
      
      HashSet<Character> filtered = (HashSet<Character>)allMortals.Where(x => x.Stand.Equivalent(stand));
      
      /*
      foreach(Character character in allMortals){
        if(character.Stand.Equivalent(stand)){
          filtered.Add(character);
        }
      }
      */
      
      return filtered;
    }
    
    
    public HashSet<Character> GetClanMembers(Clan clan){
      HashSet<Character> clanMembers = new HashSet<Character>();
      
      foreach(Character vampire in characters){
        if(vampire.IsVampire() && vampire.Clan == clan){
          clanMembers.Add(vampire);
        }
      }
      
      return clanMembers;
    }
    
    
    public void SetProtagonist(Character character, int index){
      if(index < 1 ||
        index > numberOfPlayers){
        // Todo: raise some exception
        return;
      }
      
      protagonists[index] = character;
    }
    
    
  }
}