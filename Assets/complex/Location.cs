using System;
using Vanitas.Components.Attributes;
using Vanitas.Components.Locale;

namespace Vanitas.Components{
  public class Location{
    
    private String name;
    private LocationType locationType;
    private LocationMap map;
    private Stand stand;
    
    
    public Location(LocationType locationType){
      this.locationType = locationType;
      
      this.stand = locationType.Data().GenerateStand();
      this.map = locationType.Data().GenerateMap();
      this.name = locationType.Data().GenerateName();
    }
    
  }
}