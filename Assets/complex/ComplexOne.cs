namespace Vanitas.Test{
  public class ComplexOne{
    
    private int number;
    
    public ComplexOne(int number){
      this.number = number;
    }
    
    public int Number{
      get{
        return number;
      }
    }
    
  }
}